#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<blis.h>

#include "../normConv/normConvNHWC.h"
#include "../WConv_3x3/WConv_3x3.h"

// NHWC
#define imageNHWC(n, c, i, j,    w, h, nc) (image[ ((n)*(nc*h*w)) + ((i)*(w*nc)) + ((j)*(nc)) + (c) ])
#define image_rNHWC(n, c, i, j,    w, h, nc) (image_r[ ((n)*(nc*h*w)) + ((i)*(w*nc)) + ((j)*(nc)) + (c) ])

float* gen_image(int n, float max_num);
float *zero_padNHWC(float *image_r, int H, int W, int C, int N );
float * im2row(float *x, int N, int C, int H, int W, int kh, int kw);
void print_matrix(float* image, int h, int w, int k, int n);
int compare(float* image, float* res, int n);
int i2c(float *image, float *filter, float * res,  int n, int c, int h, int w, int k, int r, int s);

int main()
{
    int n = 30;
    int k = 64;
    int c = 64;
    int h = 224;
    int w = 224;
    
    int r = 3;
    int s = 3;

    
    float *image = gen_image((n*c*h*w), 5.0);
    float *filter = gen_image((k*c*r*s), 5.0);

    float* res_w = calloc(h*w*k*n, 4);

    float *res = calloc(h*w*k*n , 4);

    
    double start, end;
    start = omp_get_wtime();
    i2c(image, filter, res, n, c, h, w, k, r, s);
    end = omp_get_wtime();

    double im2time = end - start;

    float *image_pad = zero_padNHWC(image, h, w, c, n);
    
    
    start = omp_get_wtime();
     WConv_3x3(image_pad, filter, res_w,  h+2, w+2, c, k, n, 3);
    end = omp_get_wtime();
    
    double wintime = end - start;


    compare(res_w, res, n*k*h*w);
    printf("      | I2C   | Winograd |\n");
    printf("Times | %0.2lf | %0.2lf  |\n", im2time, wintime );


    

}

int i2c(float *image, float *filter, float *res, int n, int c, int h, int w, int k, int r, int s)
{
    //i2c
    int vdilation = 1;
    int hdilation = 1;
    int vpadding = 1;
    int hpadding = 1;
    int vstride = 1;
    int hstride = 1;

    // print_matrix(image, h, w, c, n);
    // print_matrix(filter, r, s, c, k);

    int hh = (h + 2 * vpadding - vdilation * (r - 1) - 1) / vstride + 1;
    int ww = (w + 2 * hpadding - hdilation * (s - 1) - 1) / hstride + 1;

    float *image_t2 = im2row(image, n, c, h, w, r, s);
    
    // print_matrix(image_t2, n * hh * ww,  c * r * s, 1, n);
    
 
    

    inc_t rowsA = n * hh * ww ;
    inc_t colsB = k;
    inc_t common =  c * r * s;
     
    inc_t rsa, csa;
    inc_t rsb, csb;
    inc_t rsc, csc;

    rsc = colsB; csc = 1;
    rsa = common; csa = 1;
    rsb = colsB; csb = 1;   

    float alpha, beta;
    alpha = 1.0;
    beta = 0.0;     
        
    
    bli_sgemm(BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE, rowsA, colsB, common, &alpha, 
            image_t2, rsa, csa, 
            filter, rsb, csb, 
            &beta, 
            res, rsc, csc);
    
    return 0;

}


float* gen_image(int n, float max_num)
{
    float *image = (float *) calloc(n,sizeof(float));
    for(int i=0; i< n; i++)  
        // image[i] = i;
        image[i] = ((float)rand()/(float)(RAND_MAX)) * max_num;
    return image; 
}


float *zero_padNHWC(float *image_r, int H, int W, int C, int N )
{
    int old_H,old_W;
    old_H = H;
    old_W = W;
    H = H + 2;
    W = W + 2;
    float *image = (float *) calloc(H*W*C*N, 4);
      for(int n=0; n < N; n++)
        for(int c=0; c < C; c++)
            for(int h=0; h < old_H; h++)
                for(int w=0; w < old_W; w++)
                {
                    imageNHWC(n,c,(h+1),(w+1),   W,H,C) = image_rNHWC(n,c,h,w,  old_W, old_H,C);
                }
    return image;
}



float* im2row( float *x, int N, int C, int H, int W, int kh, int kw)
{
    int vdilation = 1;
    int hdilation = 1;
    int vpadding = 1;
    int hpadding = 1;
    int vstride = 1;
    int hstride = 1;
    

    int hh = (H + 2 * vpadding - vdilation * (kh - 1) - 1) / vstride + 1;
    int ww = (W + 2 * hpadding - hdilation * (kw - 1) - 1) / hstride + 1;

    int nn, xx, yy, row, cc, ii, jj, col, x_x, x_y;

    float *rows = calloc((N * hh * ww   *   C * kh * kw), 4);

    #pragma omp parallel for
    for(int nn=0; nn<N; nn++)
        for(int xx=0; xx<hh; xx++)
        for(int yy=0; yy<ww; yy++)
        {
            row = nn * hh * ww + xx * ww + yy;
            for(int cc=0; cc<C; cc++)
                for(int ii=0; ii<kh; ii++)
                {
                    x_x = vstride * xx + vdilation * ii - vpadding;
                    if((0 <= x_x) && (x_x < H))
                        for(int jj=0; jj<kw; jj++)
                        {
                            x_y = hstride * yy + hdilation * jj - hpadding;
                            if( (0<= x_y) && (x_y < W))
                            {
                                col = cc * kh * kw + ii * kw + jj;
                                // printf("|| %d\n",(nn)*H*W*C + (x_x)*C*W + (x_y)*C + cc);
                                rows[row*(C*kh*kw) + col] = x[(nn)*H*W*C + (x_x)*C*W + (x_y)*C + cc];
                            }
                        }
                }
        }
    return rows;       
}

int compare(float* image, float* res, int n)
{
    for(int i=0; i<n; i++) 
        if (abs(image[i] - res[i]) > 1e-5)
        {
            printf("\033[31mImages are NOT equal\033[39m\n");
            return 1;
        }
    
    printf("\033[32mImages are equal\033[39m\n");
    return 0;
}

void print_matrix(float* image, int h, int w, int k, int n)
{
    for(int a = 0; a < n; a++)
    {
        for(int b = 0; b < k; b++)
        {
            for(int i=0; i<h; i++)
            {
                for(int j=0; j<w; j++)
                {
                printf("%0.2f ",imageNHWC(a,b,i,j,  w,h,k));
                }
                printf("\n");
            }
            // printf("\n------------kc-------------------\n");
            printf("\n");
        }
        printf("\n");

            // printf("\n------------NNNNNNN-------------------\n");
    }
    fflush(NULL);
    return;
}
