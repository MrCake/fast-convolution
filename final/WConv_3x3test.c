#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<cblas.h>
#include <arm_neon.h>

#include "./normConv/normConv.h"
#include "./normConv/normConvNHWC.h"
#include "./WConv_3x3/WConv_3x3.h"


float* gen_image(int n, float max_num);
void print_matrix(float* image, int h, int w, int k, int n);
int compare(float* image, float* res, int n);
float *zero_pad(float *image_r, int H, int W, int C, int N );
float *zero_padNHWC(float *image_r, int H, int W, int C, int N );

#define image_rNHWC(n, c, i, j,    w, h, nc) (image_r[ ((n)*(nc*h*w)) + ((i)*(w*nc)) + ((j)*(nc)) + (c) ])

int main(int argc, char** argv) {    
    
    int H_val[16] = {512 , 512, 513, 513, 512 , 512, 513, 513,
                   512 , 512, 513, 513, 512 , 512, 513, 513};
    int W_val[16] = {512 , 513, 512, 513, 512 , 513, 512, 513,
                   512 , 513, 512, 513, 512 , 513, 512, 513};
    int C_val[16] = {3   , 3  , 3  , 3,   4   , 4 ,  4  , 4,
                   3   , 3  , 3  , 3,   4   , 4 ,  4  , 4};
    int K_val[16] = {3   , 3  , 3  , 3,   3   , 3 ,  3  , 3,
                   4   , 4  , 4  , 4,   4   , 4 ,  4  , 4};
    int N = 22; 
    
    printf("Testing NCHW - CKRS - NKHW\n");
    for(int i=0; i< 16; i++)
    {
        // printf("\nTest %d:  \n",i);
        //                       n    c           h          w
        float* image = gen_image(N * C_val[i] * H_val[i] * W_val[i], 2.0);
        //                        k          c          r   s
        float* filter = gen_image(K_val[i] * C_val[i] * 3 * 3,     2.0);

        float *wnn_res = (float *) calloc((H_val[i])*(W_val[i])*K_val[i]*N , 4);
        float *image_pad = zero_pad(image, H_val[i], W_val[i], C_val[i], N);

        float* res = normConv(image, filter, H_val[i], W_val[i], 3, 3, C_val[i], K_val[i], N);
        WConv_3x3(image_pad, filter, wnn_res,  H_val[i]+2, W_val[i]+2, C_val[i], K_val[i], N, 0);

        compare(res,wnn_res,N*K_val[i]*H_val[i]*W_val[i]);
        
        
        WConv_3x3(image_pad, filter, wnn_res,  H_val[i]+2, W_val[i]+2, C_val[i], K_val[i], N, 1);

        compare(res,wnn_res,N*K_val[i]*H_val[i]*W_val[i]);

        free(image);
        free(filter);
        free(image_pad);
        free(res);
        free(wnn_res);
        
    }

    printf("Testing NHWC - CRSK - NHWK\n");

        for(int i=0; i< 16; i++)
    {
        // printf("\nTest %d:  \n",i);
        //                       n    c           h          w
        float* image = gen_image(N * C_val[i] * H_val[i] * W_val[i], 2.0);
        //                        k          c          r   s
        float* filter = gen_image(K_val[i] * C_val[i] * 3 * 3,     2.0);

        float *wnn_res = (float *) calloc((H_val[i])*(W_val[i])*K_val[i]*N , 4);
        float *image_pad = zero_padNHWC(image, H_val[i], W_val[i], C_val[i], N);

        float* res = normConvNHWC(image, filter, H_val[i], W_val[i], 3, 3, C_val[i], K_val[i], N);
        WConv_3x3(image_pad, filter, wnn_res,  H_val[i]+2, W_val[i]+2, C_val[i], K_val[i], N, 2);

        compare(res,wnn_res,N*K_val[i]*H_val[i]*W_val[i]);
        
        
        WConv_3x3(image_pad, filter, wnn_res,  H_val[i]+2, W_val[i]+2, C_val[i], K_val[i], N, 3);

        compare(res,wnn_res,N*K_val[i]*H_val[i]*W_val[i]);

        free(image);
        free(filter);
        free(image_pad);
        free(res);
        free(wnn_res);
        
    }

        printf("Testing Bulk GEMM NCHW\n");
    for(int i=0; i< 16; i+=4)
    {
        // printf("\nTest %d:  \n",i);
        //                       n    c           h          w
        float* image = gen_image(N * C_val[i] * H_val[i] * W_val[i], 2.0);
        //                        k          c          r   s
        float* filter = gen_image(K_val[i] * C_val[i] * 3 * 3,     2.0);

        float *wnn_res = (float *) calloc((H_val[i])*(W_val[i])*K_val[i]*N , 4);
        float *image_pad = zero_pad(image, H_val[i], W_val[i], C_val[i], N);

        float* res = normConv(image, filter, H_val[i], W_val[i], 3, 3, C_val[i], K_val[i], N);
        WConv_3x3(image_pad, filter, wnn_res,  H_val[i]+2, W_val[i]+2, C_val[i], K_val[i], N, 11);

        compare(res,wnn_res,N*K_val[i]*H_val[i]*W_val[i]);

        free(image);
        free(filter);
        free(image_pad);
        free(res);
        free(wnn_res);
        
    }


     printf("Testing Bulk GEMM NHWC\n");

        for(int i=0; i< 16; i+=4)
    {
        // printf("\nTest %d:  \n",i);
        //                       n    c           h          w
        float* image = gen_image(N * C_val[i] * H_val[i] * W_val[i], 2.0);
        //                        k          c          r   s
        float* filter = gen_image(K_val[i] * C_val[i] * 3 * 3,     2.0);

        float *wnn_res = (float *) calloc((H_val[i])*(W_val[i])*K_val[i]*N , 4);
        float *image_pad = zero_padNHWC(image, H_val[i], W_val[i], C_val[i], N);    

        float* res = normConvNHWC(image, filter, H_val[i], W_val[i], 3, 3, C_val[i], K_val[i], N);
        WConv_3x3(image_pad, filter, wnn_res,  H_val[i]+2, W_val[i]+2, C_val[i], K_val[i], N, 33);

        compare(res,wnn_res,N*K_val[i]*H_val[i]*W_val[i]);

        free(image);
        free(filter);
        free(image_pad);
        free(res);
        free(wnn_res);
        
    }
         
    return 0;
}



float *zero_pad(float *image_r, int H, int W, int C, int N )
{
    int old_H,old_W;
    old_H = H;
    old_W = W;
    H = H + 2;
    W = W + 2;
    float *image = (float *) calloc(H*W*C*N, 4);
    for(int n=0; n < N; n++)
        for(int c=0; c < C; c++)
            for(int h=0; h < old_H; h++)
                for(int w=0; w < old_W; w++)
                {
                    image[(w + 1) + ((h + 1)*W) + (c*W*H) + (n*W*H*C)] = image_r[w + (h*old_W) + (c*old_W*old_H) + (n*old_W*old_H*C)];
                }
    return image;
}
  


float *zero_padNHWC(float *image_r, int H, int W, int C, int N )
{
    int old_H,old_W;
    old_H = H;
    old_W = W;
    H = H + 2;
    W = W + 2;
    float *image = (float *) calloc(H*W*C*N, 4);
      for(int n=0; n < N; n++)
        for(int c=0; c < C; c++)
            for(int h=0; h < old_H; h++)
                for(int w=0; w < old_W; w++)
                {
                    imageNHWC(n,c,(h+1),(w+1),   W,H,C) = image_rNHWC(n,c,h,w,  old_W, old_H,C);
                }
    return image;
}

int compare(float* image, float* res, int n)
{
    for(int i=0; i<n; i++) 
        if (abs(image[i] - res[i]) > 1e-5)
        {
            printf("\033[31mImages are NOT equal\033[39m\n");
            return 1;
        }
    
    printf("\033[32mImages are equal\033[39m\n");
    return 0;
}

float* gen_image(int n, float max_num)
{
    float *image = (float *) calloc(n,sizeof(float));
    for(int i=0; i< n; i++)  
        //  image[i] = i+1;
        image[i] = ((float)rand()/(float)(RAND_MAX)) * max_num;
    return image; 
}

void print_matrix(float* image, int h, int w, int k, int n)
{
    for(int a = 0; a < n; a++)
    {
        for(int b = 0; b < k; b++)
        {
            for(int i=0; i<h; i++)
            {
                for(int j=0; j<w; j++)
                {
                printf("%0.2f ",imageNHWC(a,b,i,j,  w,h,k));
                }
                printf("\n");
            }
            // printf("\n------------kc-------------------\n");
            printf("\n");
        }
        printf("\n");

            // printf("\n------------NNNNNNN-------------------\n");
    }
    fflush(NULL);
    return;
}

