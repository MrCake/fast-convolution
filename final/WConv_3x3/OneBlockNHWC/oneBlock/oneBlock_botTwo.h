
extern inline void sub_oneBlock_botTwoNHWC(float *image, float *filter_transform, float *res,  int W,int H,int C,int K,int N)
{
    int index, despl;
     float input[16];
    float semi_image_transform[16];
    float32x4_t image_transform[16];
    float32x4_t filter_transform_i[16];
    float32x4_t *transformed_convolution = (float32x4_t *) calloc(K*4 ,4*4);
    float *image_transform_buffer = (float *) calloc(8*C ,4);
    float transformed_convolution_r[4*16];
    float image_transform_r[16];
    int  kernel_v = (K/4)*4;

    #pragma omp for collapse(2) nowait
    for(int n=0; n < N; n++)
       for(int py=0; py < H-4; py+=2)
        {
            int px = 0;

            input[0] =imageNHWC(n,0,py,px,   W,H,C);input[1] = (imageNHWC(n,0,py,px+1,   W,H,C));
            input[2] =imageNHWC(n,0,py,px+2,   W,H,C);input[3] = (imageNHWC(n,0,py,px+3,   W,H,C));

            input[4] = (imageNHWC(n,0,py+1,px,   W,H,C));input[5] = (imageNHWC(n,0,py+1,px+1,   W,H,C));
            input[6] = (imageNHWC(n,0,py+1,px+2,   W,H,C));input[7] = (imageNHWC(n,0,py+1,px+3,   W,H,C));

            input[8] = (imageNHWC(n,0,py+2,px,   W,H,C));input[9] = (imageNHWC(n,0,py+2,px+1,   W,H,C));
            input[10] = (imageNHWC(n,0,py+2,px+2,   W,H,C));input[11] = (imageNHWC(n,0,py+2,px+3,   W,H,C));

            input[12] = (imageNHWC(n,0,py+3,px,   W,H,C));input[13] = (imageNHWC(n,0,py+3,px+1,   W,H,C));
            input[14] = (imageNHWC(n,0,py+3,px+2,   W,H,C));input[15] = (imageNHWC(n,0,py+3,px+3,   W,H,C));

            semi_image_transform[0] = (input[0] - input[8]);semi_image_transform[1] = (input[1] - input[9]);
            semi_image_transform[2] = (input[2] - input[10]);semi_image_transform[3] = (input[3] - input[11]);

            semi_image_transform[4] = (input[4] + input[8]);semi_image_transform[5] = (input[5] + input[9]);
            semi_image_transform[6] = (input[6] + input[10]);semi_image_transform[7] = (input[7] + input[11]);

            semi_image_transform[8] = (input[8] - input[4]);semi_image_transform[9] = (input[9] - input[5]);
            semi_image_transform[10] = (input[10] - input[6]);semi_image_transform[11] = (input[11] - input[7]);

            semi_image_transform[12] = (input[4] - input[12]);semi_image_transform[13] = (input[5] - input[13]);
            semi_image_transform[14] = (input[6] - input[14]);semi_image_transform[15] = (input[7] - input[15]);

            image_transform_buffer[0] = semi_image_transform[2];image_transform_buffer[1] = semi_image_transform[3];
            image_transform_buffer[2] = semi_image_transform[6];image_transform_buffer[3] = semi_image_transform[7];
            image_transform_buffer[4] = semi_image_transform[10];image_transform_buffer[5] = semi_image_transform[11];
            image_transform_buffer[6] = semi_image_transform[14];image_transform_buffer[7] = semi_image_transform[15];


            image_transform_r[0] = (semi_image_transform[0] - semi_image_transform[2]);image_transform_r[4] = (semi_image_transform[4] - semi_image_transform[6]);
            image_transform_r[8] = (semi_image_transform[8] - semi_image_transform[10]);image_transform_r[12] = (semi_image_transform[12] - semi_image_transform[14]);

            image_transform_r[1] = (semi_image_transform[1] + semi_image_transform[2]);image_transform_r[5] = (semi_image_transform[5] + semi_image_transform[6]);
            image_transform_r[9] = (semi_image_transform[9] + semi_image_transform[10]);image_transform_r[13] = (semi_image_transform[13] + semi_image_transform[14]);

            image_transform_r[2] = (semi_image_transform[2] - semi_image_transform[1]);image_transform_r[6] = (semi_image_transform[6] - semi_image_transform[5]);
            image_transform_r[10] = (semi_image_transform[10] - semi_image_transform[9]);image_transform_r[14] = (semi_image_transform[14] - semi_image_transform[13]);

            image_transform_r[3] = (semi_image_transform[1] - semi_image_transform[3]);image_transform_r[7] = (semi_image_transform[5] - semi_image_transform[7]);
            image_transform_r[11] = (semi_image_transform[9] - semi_image_transform[11]);image_transform_r[15] = (semi_image_transform[13] - semi_image_transform[15]);

            for(int zz=0; zz < 16; ++zz)
                image_transform[zz] = vmovq_n_f32(image_transform_r[zz]);


            for(int k=0; k < kernel_v; k+=4)
            {
                index = k*4;

                for(int zz=0; zz < 16; zz++)
                    filter_transform_i[zz] = vld1q_f32(filter_transform + (k) + (K*zz));

                for(int zz=0; zz < 16; ++zz)
                    transformed_convolution[zz + index] = vmulq_f32(image_transform[zz], filter_transform_i[zz]);
            }

            for(int k=kernel_v; k < K; k++)
                for(int zz=0; zz < 16; ++zz)
                    transformed_convolution_r[zz + ((k-kernel_v)*16)] = 
                                image_transform_r[zz] * filter_transform[k + (zz*K)];
            
            for(int c=1; c < C ; c++)
            {
                input[0] =imageNHWC(n,c,py,px,   W,H,C);input[1] = (imageNHWC(n,c,py,px+1,   W,H,C));
                input[2] =imageNHWC(n,c,py,px+2,   W,H,C);input[3] = (imageNHWC(n,c,py,px+3,   W,H,C));

                input[4] = (imageNHWC(n,c,py+1,px,   W,H,C));input[5] = (imageNHWC(n,c,py+1,px+1,   W,H,C));
                input[6] = (imageNHWC(n,c,py+1,px+2,   W,H,C));input[7] = (imageNHWC(n,c,py+1,px+3,   W,H,C));

                input[8] = (imageNHWC(n,c,py+2,px,   W,H,C));input[9] = (imageNHWC(n,c,py+2,px+1,   W,H,C));
                input[10] = (imageNHWC(n,c,py+2,px+2,   W,H,C));input[11] = (imageNHWC(n,c,py+2,px+3,   W,H,C));

                input[12] = (imageNHWC(n,c,py+3,px,   W,H,C));input[13] = (imageNHWC(n,c,py+3,px+1,   W,H,C));
                input[14] = (imageNHWC(n,c,py+3,px+2,   W,H,C));input[15] = (imageNHWC(n,c,py+3,px+3,   W,H,C));

                semi_image_transform[0] = (input[0] - input[8]);semi_image_transform[1] = (input[1] - input[9]);
                semi_image_transform[2] = (input[2] - input[10]);semi_image_transform[3] = (input[3] - input[11]);

                semi_image_transform[4] = (input[4] + input[8]);semi_image_transform[5] = (input[5] + input[9]);
                semi_image_transform[6] = (input[6] + input[10]);semi_image_transform[7] = (input[7] + input[11]);

                semi_image_transform[8] = (input[8] - input[4]);semi_image_transform[9] = (input[9] - input[5]);
                semi_image_transform[10] = (input[10] - input[6]);semi_image_transform[11] = (input[11] - input[7]);

                semi_image_transform[12] = (input[4] - input[12]);semi_image_transform[13] = (input[5] - input[13]);
                semi_image_transform[14] = (input[6] - input[14]);semi_image_transform[15] = (input[7] - input[15]);

                image_transform_buffer[(c*8)] = semi_image_transform[2];image_transform_buffer[(c*8) + 1] = semi_image_transform[3];
                image_transform_buffer[(c*8) + 2] = semi_image_transform[6];image_transform_buffer[(c*8) + 3] = semi_image_transform[7];
                image_transform_buffer[(c*8) + 4] = semi_image_transform[10];image_transform_buffer[(c*8) + 5] = semi_image_transform[11];
                image_transform_buffer[(c*8) + 6] = semi_image_transform[14];image_transform_buffer[(c*8) + 7] = semi_image_transform[15];

                image_transform_r[0] = (semi_image_transform[0] - semi_image_transform[2]);image_transform_r[4] = (semi_image_transform[4] - semi_image_transform[6]);
                image_transform_r[8] = (semi_image_transform[8] - semi_image_transform[10]);image_transform_r[12] = (semi_image_transform[12] - semi_image_transform[14]);

                image_transform_r[1] = (semi_image_transform[1] + semi_image_transform[2]);image_transform_r[5] = (semi_image_transform[5] + semi_image_transform[6]);
                image_transform_r[9] = (semi_image_transform[9] + semi_image_transform[10]);image_transform_r[13] = (semi_image_transform[13] + semi_image_transform[14]);

                image_transform_r[2] = (semi_image_transform[2] - semi_image_transform[1]);image_transform_r[6] = (semi_image_transform[6] - semi_image_transform[5]);
                image_transform_r[10] = (semi_image_transform[10] - semi_image_transform[9]);image_transform_r[14] = (semi_image_transform[14] - semi_image_transform[13]);

                image_transform_r[3] = (semi_image_transform[1] - semi_image_transform[3]);image_transform_r[7] = (semi_image_transform[5] - semi_image_transform[7]);
                image_transform_r[11] = (semi_image_transform[9] - semi_image_transform[11]);image_transform_r[15] = (semi_image_transform[13] - semi_image_transform[15]);

                for(int zz=0; zz < 16; ++zz)
                    image_transform[zz] = vmovq_n_f32(image_transform_r[zz]);

                despl = 16*c*K;
                for(int k=0; k < kernel_v; k+=4)
                {
                    index = k*4;
                    
                    
                    for(int zz=0; zz < 16; ++zz)
                        filter_transform_i[zz] = vld1q_f32(filter_transform + (k) + (zz*K) + (despl));
                    
                    for(int zz=0; zz < 16; ++zz)
                        transformed_convolution[zz + index] = vfmaq_f32(transformed_convolution[zz + index], image_transform[zz], filter_transform_i[zz]);
                    
                }

                for(int k=kernel_v; k < K; k++)
                    for(int zz=0; zz < 16; ++zz)
                        transformed_convolution_r[zz + ((k-kernel_v)*16)] += 
                            image_transform_r[zz] * filter_transform[k + (zz*K) + (despl)];  
            
            }

            sub_final_transformNHWC(transformed_convolution, transformed_convolution_r, res, K, H, W,  n, px, py);
        
            for(int px=2; px < W-2 ; px+=2)
            {
                input[2] = imageNHWC(n,0, py ,px+2,   W,H,C);input[3] = (imageNHWC(n,0,py,px+3,   W,H,C));
                input[6] = (imageNHWC(n,0, py+1 ,px+2,   W,H,C));input[7] = (imageNHWC(n,0,py+1,px+3,   W,H,C));
                input[10] = (imageNHWC(n,0, py+2 ,px+2,   W,H,C));input[11] = (imageNHWC(n,0,py+2,px+3,   W,H,C));
                input[14] = (imageNHWC(n,0, py+3 ,px+2,   W,H,C));input[15] = (imageNHWC(n,0,py+3,px+3,   W,H,C));

                semi_image_transform[0] = image_transform_buffer[0]; semi_image_transform[1] = image_transform_buffer[1];
                semi_image_transform[4] = image_transform_buffer[2]; semi_image_transform[5] = image_transform_buffer[3];
                semi_image_transform[8] = image_transform_buffer[4]; semi_image_transform[9] = image_transform_buffer[5];
                semi_image_transform[12] = image_transform_buffer[6]; semi_image_transform[13] = image_transform_buffer[7];

                semi_image_transform[2] = (input[2] - input[10]);semi_image_transform[3] = (input[3] - input[11]);
                semi_image_transform[6] = (input[6] + input[10]);semi_image_transform[7] = (input[7] + input[11]);
                semi_image_transform[10] = (input[10] - input[6]);semi_image_transform[11] = (input[11] - input[7]);
                semi_image_transform[14] = (input[6] - input[14]);semi_image_transform[15] = (input[7] - input[15]);

            
                image_transform_buffer[0] = semi_image_transform[2];image_transform_buffer[1] = semi_image_transform[3];
                image_transform_buffer[2] = semi_image_transform[6];image_transform_buffer[3] = semi_image_transform[7];
                image_transform_buffer[4] = semi_image_transform[10];image_transform_buffer[5] = semi_image_transform[11];
                image_transform_buffer[6] = semi_image_transform[14];image_transform_buffer[7] = semi_image_transform[15];


                image_transform_r[0] = (semi_image_transform[0] - semi_image_transform[2]);image_transform_r[4] = (semi_image_transform[4] - semi_image_transform[6]);
                image_transform_r[8] = (semi_image_transform[8] - semi_image_transform[10]);image_transform_r[12] = (semi_image_transform[12] - semi_image_transform[14]);

                image_transform_r[1] = (semi_image_transform[1] + semi_image_transform[2]);image_transform_r[5] = (semi_image_transform[5] + semi_image_transform[6]);
                image_transform_r[9] = (semi_image_transform[9] + semi_image_transform[10]);image_transform_r[13] = (semi_image_transform[13] + semi_image_transform[14]);

                image_transform_r[2] = (semi_image_transform[2] - semi_image_transform[1]);image_transform_r[6] = (semi_image_transform[6] - semi_image_transform[5]);
                image_transform_r[10] = (semi_image_transform[10] - semi_image_transform[9]);image_transform_r[14] = (semi_image_transform[14] - semi_image_transform[13]);

                image_transform_r[3] = (semi_image_transform[1] - semi_image_transform[3]);image_transform_r[7] = (semi_image_transform[5] - semi_image_transform[7]);
                image_transform_r[11] = (semi_image_transform[9] - semi_image_transform[11]);image_transform_r[15] = (semi_image_transform[13] - semi_image_transform[15]);

                for(int zz=0; zz < 16; ++zz)
                    image_transform[zz] = vmovq_n_f32(image_transform_r[zz]);


                for(int k=0; k < kernel_v; k+=4)
                {
                    index = k*4;

                    for(int zz=0; zz < 16; zz++)
                        filter_transform_i[zz] = vld1q_f32(filter_transform + (k) + (K*zz));

                    for(int zz=0; zz < 16; ++zz)
                        transformed_convolution[zz + index] = vmulq_f32(image_transform[zz], filter_transform_i[zz]);
                }

                for(int k=kernel_v; k < K; k++)
                    for(int zz=0; zz < 16; ++zz)
                        transformed_convolution_r[zz + ((k-kernel_v)*16)] = 
                                    image_transform_r[zz] * filter_transform[k + (zz*K)];
                
                for(int c=1; c < C ; c++)
                {
                    input[2] =imageNHWC(n,c,py,px+2,   W,H,C);input[3] = (imageNHWC(n,c,py,px+3,   W,H,C));
                    input[6] = (imageNHWC(n,c,py+1,px+2,   W,H,C));input[7] = (imageNHWC(n,c,py+1,px+3,   W,H,C));
                    input[10] = (imageNHWC(n,c,py+2,px+2,   W,H,C));input[11] = (imageNHWC(n,c,py+2,px+3,   W,H,C));
                    input[14] = (imageNHWC(n,c,py+3,px+2,   W,H,C));input[15] = (imageNHWC(n,c,py+3,px+3,   W,H,C));

                    semi_image_transform[0] = image_transform_buffer[(c*8)]; semi_image_transform[1] = image_transform_buffer[(c*8) + 1];
                    semi_image_transform[4] = image_transform_buffer[(c*8) + 2]; semi_image_transform[5] = image_transform_buffer[(c*8) + 3];
                    semi_image_transform[8] = image_transform_buffer[(c*8) + 4]; semi_image_transform[9] = image_transform_buffer[(c*8) + 5];
                    semi_image_transform[12] = image_transform_buffer[(c*8) + 6]; semi_image_transform[13] = image_transform_buffer[(c*8) + 7];

                    semi_image_transform[2] = (input[2] - input[10]);semi_image_transform[3] = (input[3] - input[11]);
                    semi_image_transform[6] = (input[6] + input[10]);semi_image_transform[7] = (input[7] + input[11]);
                    semi_image_transform[10] = (input[10] - input[6]);semi_image_transform[11] = (input[11] - input[7]);
                    semi_image_transform[14] = (input[6] - input[14]);semi_image_transform[15] = (input[7] - input[15]);

                    image_transform_buffer[(c*8)] = semi_image_transform[2];image_transform_buffer[(c*8) + 1] = semi_image_transform[3];
                    image_transform_buffer[(c*8) + 2] = semi_image_transform[6];image_transform_buffer[(c*8) + 3] = semi_image_transform[7];
                    image_transform_buffer[(c*8) + 4] = semi_image_transform[10];image_transform_buffer[(c*8) + 5] = semi_image_transform[11];
                    image_transform_buffer[(c*8) + 6] = semi_image_transform[14];image_transform_buffer[(c*8) + 7] = semi_image_transform[15];

                    image_transform_r[0] = (semi_image_transform[0] - semi_image_transform[2]);image_transform_r[4] = (semi_image_transform[4] - semi_image_transform[6]);
                    image_transform_r[8] = (semi_image_transform[8] - semi_image_transform[10]);image_transform_r[12] = (semi_image_transform[12] - semi_image_transform[14]);

                    image_transform_r[1] = (semi_image_transform[1] + semi_image_transform[2]);image_transform_r[5] = (semi_image_transform[5] + semi_image_transform[6]);
                    image_transform_r[9] = (semi_image_transform[9] + semi_image_transform[10]);image_transform_r[13] = (semi_image_transform[13] + semi_image_transform[14]);

                    image_transform_r[2] = (semi_image_transform[2] - semi_image_transform[1]);image_transform_r[6] = (semi_image_transform[6] - semi_image_transform[5]);
                    image_transform_r[10] = (semi_image_transform[10] - semi_image_transform[9]);image_transform_r[14] = (semi_image_transform[14] - semi_image_transform[13]);

                    image_transform_r[3] = (semi_image_transform[1] - semi_image_transform[3]);image_transform_r[7] = (semi_image_transform[5] - semi_image_transform[7]);
                    image_transform_r[11] = (semi_image_transform[9] - semi_image_transform[11]);image_transform_r[15] = (semi_image_transform[13] - semi_image_transform[15]);

                    for(int zz=0; zz < 16; ++zz)
                        image_transform[zz] = vmovq_n_f32(image_transform_r[zz]);

                    despl = 16*c*K;
                    for(int k=0; k < kernel_v; k+=4)
                    {
                        index = k*4;
                       
                        
                        for(int zz=0; zz < 16; ++zz)
                            filter_transform_i[zz] = vld1q_f32(filter_transform + (k) + (zz*K) + (despl));
                        
                        for(int zz=0; zz < 16; ++zz)
                            transformed_convolution[zz + index] = vfmaq_f32(transformed_convolution[zz + index], image_transform[zz], filter_transform_i[zz]);
                        
                    }

                    for(int k=kernel_v; k < K; k++)
                        for(int zz=0; zz < 16; ++zz)
                            transformed_convolution_r[zz + ((k-kernel_v)*16)] += 
                                image_transform_r[zz] * filter_transform[k + (zz*K) + (despl)];  
                
                }

                sub_final_transformNHWC(transformed_convolution, transformed_convolution_r, res, K, H, W,  n, px, py);
            }
        }
    #pragma omp for
    for(int n=0; n < N; n++)
    {
        int py = H-4;
        int px = 0;

        input[0] =imageNHWC(n,0,py,px,   W,H,C);input[1] = (imageNHWC(n,0,py,px+1,   W,H,C));
        input[2] =imageNHWC(n,0,py,px+2,   W,H,C);input[3] = (imageNHWC(n,0,py,px+3,   W,H,C));

        input[4] = (imageNHWC(n,0,py+1,px,   W,H,C));input[5] = (imageNHWC(n,0,py+1,px+1,   W,H,C));
        input[6] = (imageNHWC(n,0,py+1,px+2,   W,H,C));input[7] = (imageNHWC(n,0,py+1,px+3,   W,H,C));

        input[8] = (imageNHWC(n,0,py+2,px,   W,H,C));input[9] = (imageNHWC(n,0,py+2,px+1,   W,H,C));
        input[10] = (imageNHWC(n,0,py+2,px+2,   W,H,C));input[11] = (imageNHWC(n,0,py+2,px+3,   W,H,C));

        input[12] = (imageNHWC(n,0,py+3,px,   W,H,C));input[13] = (imageNHWC(n,0,py+3,px+1,   W,H,C));
        input[14] = (imageNHWC(n,0,py+3,px+2,   W,H,C));input[15] = (imageNHWC(n,0,py+3,px+3,   W,H,C));

        semi_image_transform[0] = (input[0] - input[8]);semi_image_transform[1] = (input[1] - input[9]);
        semi_image_transform[2] = (input[2] - input[10]);semi_image_transform[3] = (input[3] - input[11]);

        semi_image_transform[4] = (input[4] + input[8]);semi_image_transform[5] = (input[5] + input[9]);
        semi_image_transform[6] = (input[6] + input[10]);semi_image_transform[7] = (input[7] + input[11]);

        semi_image_transform[8] = (input[8] - input[4]);semi_image_transform[9] = (input[9] - input[5]);
        semi_image_transform[10] = (input[10] - input[6]);semi_image_transform[11] = (input[11] - input[7]);

        semi_image_transform[12] = (input[4] - input[12]);semi_image_transform[13] = (input[5] - input[13]);
        semi_image_transform[14] = (input[6] - input[14]);semi_image_transform[15] = (input[7] - input[15]);

        image_transform_buffer[0] = semi_image_transform[2];image_transform_buffer[1] = semi_image_transform[3];
        image_transform_buffer[2] = semi_image_transform[6];image_transform_buffer[3] = semi_image_transform[7];
        image_transform_buffer[4] = semi_image_transform[10];image_transform_buffer[5] = semi_image_transform[11];
        image_transform_buffer[6] = semi_image_transform[14];image_transform_buffer[7] = semi_image_transform[15];


        image_transform_r[0] = (semi_image_transform[0] - semi_image_transform[2]);image_transform_r[4] = (semi_image_transform[4] - semi_image_transform[6]);
        image_transform_r[8] = (semi_image_transform[8] - semi_image_transform[10]);image_transform_r[12] = (semi_image_transform[12] - semi_image_transform[14]);

        image_transform_r[1] = (semi_image_transform[1] + semi_image_transform[2]);image_transform_r[5] = (semi_image_transform[5] + semi_image_transform[6]);
        image_transform_r[9] = (semi_image_transform[9] + semi_image_transform[10]);image_transform_r[13] = (semi_image_transform[13] + semi_image_transform[14]);

        image_transform_r[2] = (semi_image_transform[2] - semi_image_transform[1]);image_transform_r[6] = (semi_image_transform[6] - semi_image_transform[5]);
        image_transform_r[10] = (semi_image_transform[10] - semi_image_transform[9]);image_transform_r[14] = (semi_image_transform[14] - semi_image_transform[13]);

        image_transform_r[3] = (semi_image_transform[1] - semi_image_transform[3]);image_transform_r[7] = (semi_image_transform[5] - semi_image_transform[7]);
        image_transform_r[11] = (semi_image_transform[9] - semi_image_transform[11]);image_transform_r[15] = (semi_image_transform[13] - semi_image_transform[15]);

        for(int zz=0; zz < 16; ++zz)
            image_transform[zz] = vmovq_n_f32(image_transform_r[zz]);


        for(int k=0; k < kernel_v; k+=4)
        {
            index = k*4;

            for(int zz=0; zz < 16; zz++)
                filter_transform_i[zz] = vld1q_f32(filter_transform + (k) + (K*zz));

            for(int zz=0; zz < 16; ++zz)
                transformed_convolution[zz + index] = vmulq_f32(image_transform[zz], filter_transform_i[zz]);
        }

        for(int k=kernel_v; k < K; k++)
            for(int zz=0; zz < 16; ++zz)
                transformed_convolution_r[zz + ((k-kernel_v)*16)] = 
                            image_transform_r[zz] * filter_transform[k + (zz*K)];
        
        for(int c=1; c < C ; c++)
        {
            input[0] =imageNHWC(n,c,py,px,   W,H,C);input[1] = (imageNHWC(n,c,py,px+1,   W,H,C));
            input[2] =imageNHWC(n,c,py,px+2,   W,H,C);input[3] = (imageNHWC(n,c,py,px+3,   W,H,C));

            input[4] = (imageNHWC(n,c,py+1,px,   W,H,C));input[5] = (imageNHWC(n,c,py+1,px+1,   W,H,C));
            input[6] = (imageNHWC(n,c,py+1,px+2,   W,H,C));input[7] = (imageNHWC(n,c,py+1,px+3,   W,H,C));

            input[8] = (imageNHWC(n,c,py+2,px,   W,H,C));input[9] = (imageNHWC(n,c,py+2,px+1,   W,H,C));
            input[10] = (imageNHWC(n,c,py+2,px+2,   W,H,C));input[11] = (imageNHWC(n,c,py+2,px+3,   W,H,C));

            input[12] = (imageNHWC(n,c,py+3,px,   W,H,C));input[13] = (imageNHWC(n,c,py+3,px+1,   W,H,C));
            input[14] = (imageNHWC(n,c,py+3,px+2,   W,H,C));input[15] = (imageNHWC(n,c,py+3,px+3,   W,H,C));

            semi_image_transform[0] = (input[0] - input[8]);semi_image_transform[1] = (input[1] - input[9]);
            semi_image_transform[2] = (input[2] - input[10]);semi_image_transform[3] = (input[3] - input[11]);

            semi_image_transform[4] = (input[4] + input[8]);semi_image_transform[5] = (input[5] + input[9]);
            semi_image_transform[6] = (input[6] + input[10]);semi_image_transform[7] = (input[7] + input[11]);

            semi_image_transform[8] = (input[8] - input[4]);semi_image_transform[9] = (input[9] - input[5]);
            semi_image_transform[10] = (input[10] - input[6]);semi_image_transform[11] = (input[11] - input[7]);

            semi_image_transform[12] = (input[4] - input[12]);semi_image_transform[13] = (input[5] - input[13]);
            semi_image_transform[14] = (input[6] - input[14]);semi_image_transform[15] = (input[7] - input[15]);

            image_transform_buffer[(c*8)] = semi_image_transform[2];image_transform_buffer[(c*8) + 1] = semi_image_transform[3];
            image_transform_buffer[(c*8) + 2] = semi_image_transform[6];image_transform_buffer[(c*8) + 3] = semi_image_transform[7];
            image_transform_buffer[(c*8) + 4] = semi_image_transform[10];image_transform_buffer[(c*8) + 5] = semi_image_transform[11];
            image_transform_buffer[(c*8) + 6] = semi_image_transform[14];image_transform_buffer[(c*8) + 7] = semi_image_transform[15];

            image_transform_r[0] = (semi_image_transform[0] - semi_image_transform[2]);image_transform_r[4] = (semi_image_transform[4] - semi_image_transform[6]);
            image_transform_r[8] = (semi_image_transform[8] - semi_image_transform[10]);image_transform_r[12] = (semi_image_transform[12] - semi_image_transform[14]);

            image_transform_r[1] = (semi_image_transform[1] + semi_image_transform[2]);image_transform_r[5] = (semi_image_transform[5] + semi_image_transform[6]);
            image_transform_r[9] = (semi_image_transform[9] + semi_image_transform[10]);image_transform_r[13] = (semi_image_transform[13] + semi_image_transform[14]);

            image_transform_r[2] = (semi_image_transform[2] - semi_image_transform[1]);image_transform_r[6] = (semi_image_transform[6] - semi_image_transform[5]);
            image_transform_r[10] = (semi_image_transform[10] - semi_image_transform[9]);image_transform_r[14] = (semi_image_transform[14] - semi_image_transform[13]);

            image_transform_r[3] = (semi_image_transform[1] - semi_image_transform[3]);image_transform_r[7] = (semi_image_transform[5] - semi_image_transform[7]);
            image_transform_r[11] = (semi_image_transform[9] - semi_image_transform[11]);image_transform_r[15] = (semi_image_transform[13] - semi_image_transform[15]);

            for(int zz=0; zz < 16; ++zz)
                image_transform[zz] = vmovq_n_f32(image_transform_r[zz]);

            despl = 16*c*K;
            for(int k=0; k < kernel_v; k+=4)
            {
                index = k*4;
                
                
                for(int zz=0; zz < 16; ++zz)
                    filter_transform_i[zz] = vld1q_f32(filter_transform + (k) + (zz*K) + (despl));
                
                for(int zz=0; zz < 16; ++zz)
                    transformed_convolution[zz + index] = vfmaq_f32(transformed_convolution[zz + index], image_transform[zz], filter_transform_i[zz]);
                
            }

            for(int k=kernel_v; k < K; k++)
                for(int zz=0; zz < 16; ++zz)
                    transformed_convolution_r[zz + ((k-kernel_v)*16)] += 
                        image_transform_r[zz] * filter_transform[k + (zz*K) + (despl)];  
        
        }

        sub_final_transformNHWC(transformed_convolution, transformed_convolution_r, res, K, H, W,  n, px, py);
    
        for(int px=2; px < W-2 ; px+=2)
        {
            input[2] = imageNHWC(n,0, py ,px+2,   W,H,C);input[3] = (imageNHWC(n,0,py,px+3,   W,H,C));
            input[6] = (imageNHWC(n,0, py+1 ,px+2,   W,H,C));input[7] = (imageNHWC(n,0,py+1,px+3,   W,H,C));
            input[10] = (imageNHWC(n,0, py+2 ,px+2,   W,H,C));input[11] = (imageNHWC(n,0,py+2,px+3,   W,H,C));
            input[14] = (imageNHWC(n,0, py+3 ,px+2,   W,H,C));input[15] = (imageNHWC(n,0,py+3,px+3,   W,H,C));

            semi_image_transform[0] = image_transform_buffer[0]; semi_image_transform[1] = image_transform_buffer[1];
            semi_image_transform[4] = image_transform_buffer[2]; semi_image_transform[5] = image_transform_buffer[3];
            semi_image_transform[8] = image_transform_buffer[4]; semi_image_transform[9] = image_transform_buffer[5];
            semi_image_transform[12] = image_transform_buffer[6]; semi_image_transform[13] = image_transform_buffer[7];

            semi_image_transform[2] = (input[2] - input[10]);semi_image_transform[3] = (input[3] - input[11]);
            semi_image_transform[6] = (input[6] + input[10]);semi_image_transform[7] = (input[7] + input[11]);
            semi_image_transform[10] = (input[10] - input[6]);semi_image_transform[11] = (input[11] - input[7]);
            semi_image_transform[14] = (input[6] - input[14]);semi_image_transform[15] = (input[7] - input[15]);

        
            image_transform_buffer[0] = semi_image_transform[2];image_transform_buffer[1] = semi_image_transform[3];
            image_transform_buffer[2] = semi_image_transform[6];image_transform_buffer[3] = semi_image_transform[7];
            image_transform_buffer[4] = semi_image_transform[10];image_transform_buffer[5] = semi_image_transform[11];
            image_transform_buffer[6] = semi_image_transform[14];image_transform_buffer[7] = semi_image_transform[15];


            image_transform_r[0] = (semi_image_transform[0] - semi_image_transform[2]);image_transform_r[4] = (semi_image_transform[4] - semi_image_transform[6]);
            image_transform_r[8] = (semi_image_transform[8] - semi_image_transform[10]);image_transform_r[12] = (semi_image_transform[12] - semi_image_transform[14]);

            image_transform_r[1] = (semi_image_transform[1] + semi_image_transform[2]);image_transform_r[5] = (semi_image_transform[5] + semi_image_transform[6]);
            image_transform_r[9] = (semi_image_transform[9] + semi_image_transform[10]);image_transform_r[13] = (semi_image_transform[13] + semi_image_transform[14]);

            image_transform_r[2] = (semi_image_transform[2] - semi_image_transform[1]);image_transform_r[6] = (semi_image_transform[6] - semi_image_transform[5]);
            image_transform_r[10] = (semi_image_transform[10] - semi_image_transform[9]);image_transform_r[14] = (semi_image_transform[14] - semi_image_transform[13]);

            image_transform_r[3] = (semi_image_transform[1] - semi_image_transform[3]);image_transform_r[7] = (semi_image_transform[5] - semi_image_transform[7]);
            image_transform_r[11] = (semi_image_transform[9] - semi_image_transform[11]);image_transform_r[15] = (semi_image_transform[13] - semi_image_transform[15]);

            for(int zz=0; zz < 16; ++zz)
                image_transform[zz] = vmovq_n_f32(image_transform_r[zz]);


            for(int k=0; k < kernel_v; k+=4)
            {
                index = k*4;

                for(int zz=0; zz < 16; zz++)
                    filter_transform_i[zz] = vld1q_f32(filter_transform + (k) + (K*zz));

                for(int zz=0; zz < 16; ++zz)
                    transformed_convolution[zz + index] = vmulq_f32(image_transform[zz], filter_transform_i[zz]);
            }

            for(int k=kernel_v; k < K; k++)
                for(int zz=0; zz < 16; ++zz)
                    transformed_convolution_r[zz + ((k-kernel_v)*16)] = 
                                image_transform_r[zz] * filter_transform[k + (zz*K)];
            
            for(int c=1; c < C ; c++)
            {
                input[2] =imageNHWC(n,c,py,px+2,   W,H,C);input[3] = (imageNHWC(n,c,py,px+3,   W,H,C));
                input[6] = (imageNHWC(n,c,py+1,px+2,   W,H,C));input[7] = (imageNHWC(n,c,py+1,px+3,   W,H,C));
                input[10] = (imageNHWC(n,c,py+2,px+2,   W,H,C));input[11] = (imageNHWC(n,c,py+2,px+3,   W,H,C));
                input[14] = (imageNHWC(n,c,py+3,px+2,   W,H,C));input[15] = (imageNHWC(n,c,py+3,px+3,   W,H,C));

                semi_image_transform[0] = image_transform_buffer[(c*8)]; semi_image_transform[1] = image_transform_buffer[(c*8) + 1];
                semi_image_transform[4] = image_transform_buffer[(c*8) + 2]; semi_image_transform[5] = image_transform_buffer[(c*8) + 3];
                semi_image_transform[8] = image_transform_buffer[(c*8) + 4]; semi_image_transform[9] = image_transform_buffer[(c*8) + 5];
                semi_image_transform[12] = image_transform_buffer[(c*8) + 6]; semi_image_transform[13] = image_transform_buffer[(c*8) + 7];

                semi_image_transform[2] = (input[2] - input[10]);semi_image_transform[3] = (input[3] - input[11]);
                semi_image_transform[6] = (input[6] + input[10]);semi_image_transform[7] = (input[7] + input[11]);
                semi_image_transform[10] = (input[10] - input[6]);semi_image_transform[11] = (input[11] - input[7]);
                semi_image_transform[14] = (input[6] - input[14]);semi_image_transform[15] = (input[7] - input[15]);

                image_transform_buffer[(c*8)] = semi_image_transform[2];image_transform_buffer[(c*8) + 1] = semi_image_transform[3];
                image_transform_buffer[(c*8) + 2] = semi_image_transform[6];image_transform_buffer[(c*8) + 3] = semi_image_transform[7];
                image_transform_buffer[(c*8) + 4] = semi_image_transform[10];image_transform_buffer[(c*8) + 5] = semi_image_transform[11];
                image_transform_buffer[(c*8) + 6] = semi_image_transform[14];image_transform_buffer[(c*8) + 7] = semi_image_transform[15];

                image_transform_r[0] = (semi_image_transform[0] - semi_image_transform[2]);image_transform_r[4] = (semi_image_transform[4] - semi_image_transform[6]);
                image_transform_r[8] = (semi_image_transform[8] - semi_image_transform[10]);image_transform_r[12] = (semi_image_transform[12] - semi_image_transform[14]);

                image_transform_r[1] = (semi_image_transform[1] + semi_image_transform[2]);image_transform_r[5] = (semi_image_transform[5] + semi_image_transform[6]);
                image_transform_r[9] = (semi_image_transform[9] + semi_image_transform[10]);image_transform_r[13] = (semi_image_transform[13] + semi_image_transform[14]);

                image_transform_r[2] = (semi_image_transform[2] - semi_image_transform[1]);image_transform_r[6] = (semi_image_transform[6] - semi_image_transform[5]);
                image_transform_r[10] = (semi_image_transform[10] - semi_image_transform[9]);image_transform_r[14] = (semi_image_transform[14] - semi_image_transform[13]);

                image_transform_r[3] = (semi_image_transform[1] - semi_image_transform[3]);image_transform_r[7] = (semi_image_transform[5] - semi_image_transform[7]);
                image_transform_r[11] = (semi_image_transform[9] - semi_image_transform[11]);image_transform_r[15] = (semi_image_transform[13] - semi_image_transform[15]);

                for(int zz=0; zz < 16; ++zz)
                    image_transform[zz] = vmovq_n_f32(image_transform_r[zz]);

                despl = 16*c*K;
                for(int k=0; k < kernel_v; k+=4)
                {
                    index = k*4;
                    
                    
                    for(int zz=0; zz < 16; ++zz)
                        filter_transform_i[zz] = vld1q_f32(filter_transform + (k) + (zz*K) + (despl));
                    
                    for(int zz=0; zz < 16; ++zz)
                        transformed_convolution[zz + index] = vfmaq_f32(transformed_convolution[zz + index], image_transform[zz], filter_transform_i[zz]);
                    
                }

                for(int k=kernel_v; k < K; k++)
                    for(int zz=0; zz < 16; ++zz)
                        transformed_convolution_r[zz + ((k-kernel_v)*16)] += 
                            image_transform_r[zz] * filter_transform[k + (zz*K) + (despl)];  
            
            }

            sub_final_transformNHWC(transformed_convolution, transformed_convolution_r, res, K, H, W,  n, px, py);
        }
    }
    free(transformed_convolution);
    free(image_transform_buffer);
}