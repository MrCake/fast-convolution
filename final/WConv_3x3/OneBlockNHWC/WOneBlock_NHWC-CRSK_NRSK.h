#include"./final_transform/final_transform.h"

#include"./filter_transform/filter_transform.h"

#include"./oneBlock/oneBlock.h"
#include"./oneBlock/oneBlock_rightTwo.h"
#include"./oneBlock/oneBlock_botTwo.h"
#include"./oneBlock/oneBlock_botrightTwo.h"

extern inline void WOneBlock_NHWC_CRSK_NRSK(float* image, float* filter, float *res, int H, int W, int R, int S, int C, int K, int N)
{
    int old_H,old_W;
    old_H = H-2;
    old_W = W-2;
    

    
    float *filter_transform = (float *) calloc(4*4*C*K, 4);



    // Calculate Filter tranformation to winograd domain
    sub_filter_transformNHWC(filter, filter_transform, C, K);


    #pragma omp parallel if( (H%2 == 0) )
    {

    if((W%2 == 0) && (H%2 == 0))
    { 
        sub_oneBlockNHWC(image, filter_transform, res, W, H, C, K, N);
    }else if((W%2 == 0) || (H%2 == 0))
    {
        if(W%2 == 1)
        {
            sub_oneBlock_rightTwoNHWC(image, filter_transform, res, W, H, C, K, N);
        }
    }
    }

    #pragma omp parallel if( (W%2 == 1)  && (N >= omp_get_num_threads() ))
    {
    if((W%2 == 0) || (H%2 == 0))
    {
        if(H%2 == 1)
        {
            sub_oneBlock_botTwoNHWC(image, filter_transform, res, W, H, C, K, N);
        }


    }else if((W%2 == 1) && (H%2 == 1))
    {
        sub_oneBlock_botrightTwoNHWC(image, filter_transform, res, W, H, C, K, N);
    }
    
    }

    free(filter_transform);

   return;
}