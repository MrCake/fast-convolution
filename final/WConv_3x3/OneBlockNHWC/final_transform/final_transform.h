

extern inline void sub_final_transformNHWC(float32x4_t *transformed_convolution, float* transformed_convolution_r, float * res, int K, int H, int W, int n, int px, int py)
{
    int index;
    int displ = 1;
    int total_blocks = (((W-2)/2) * ((H-2)/2));   
    int  kernel_v = (K/4)*4;
    int total_image = total_blocks*4;

    float32x4_t semi_res_v[8];
    float semi_res[16];
    int n_res = (n*K*(H-2)*(W-2));


    for(int k=0; k < kernel_v; k+=4)
    {
        index = k*4;
        semi_res_v[0] = vaddq_f32( vaddq_f32(transformed_convolution[index], transformed_convolution[index + 4]), transformed_convolution[index + 8] );
        semi_res_v[1] = vaddq_f32( vaddq_f32(transformed_convolution[index + 1], transformed_convolution[index + 5]), transformed_convolution[index + 9] );                     
        semi_res_v[2] = vaddq_f32( vaddq_f32(transformed_convolution[index + 2], transformed_convolution[index + 6]), transformed_convolution[index + 10] );
        semi_res_v[3] = vaddq_f32( vaddq_f32(transformed_convolution[index + 3], transformed_convolution[index + 7]), transformed_convolution[index + 11] );

        semi_res_v[4] = vsubq_f32( vsubq_f32(transformed_convolution[index + 4], transformed_convolution[index + 8]), transformed_convolution[index + 12] );
        semi_res_v[5] = vsubq_f32( vsubq_f32(transformed_convolution[index + 5], transformed_convolution[index + 9]), transformed_convolution[index + 13] );                            
        semi_res_v[6] = vsubq_f32( vsubq_f32(transformed_convolution[index + 6], transformed_convolution[index + 10]), transformed_convolution[index + 14] );
        semi_res_v[7] = vsubq_f32( vsubq_f32(transformed_convolution[index + 7], transformed_convolution[index + 11]), transformed_convolution[index + 15] );

        
        //LAST
        semi_res_v[0] = vaddq_f32( vaddq_f32(semi_res_v[0], semi_res_v[1]), semi_res_v[2]);
        semi_res_v[1] = vsubq_f32( vsubq_f32(semi_res_v[1], semi_res_v[2]), semi_res_v[3]);
        semi_res_v[4] = vaddq_f32( vaddq_f32(semi_res_v[4], semi_res_v[5]), semi_res_v[6]);
        semi_res_v[5] = vsubq_f32( vsubq_f32(semi_res_v[5], semi_res_v[6]), semi_res_v[7]);

        index =  n_res;  
        for (int zz=0; zz < 4; zz++)
        {
            
        res[ (k+zz) + (px*K) + (py*(W-2)*K) + index] = vgetq_lane_f32(semi_res_v[0], zz); 
        res[ (k+zz) + ((px+1)*K) + (py*(W-2)*K) + index] =vgetq_lane_f32(semi_res_v[1],zz);
        res[ (k+zz) + (px*K) + ((py+1)*(W-2)*K) + index] =  vgetq_lane_f32(semi_res_v[4],zz);
        res[ (k+zz) + ((px+1)*K) + ((py+1)*(W-2)*K) + index] = vgetq_lane_f32(semi_res_v[5],zz);
        
        } 
    }

    for(int k= (kernel_v); k < K ; ++k)
    {
        index = (k-kernel_v)*16;

        semi_res[0] = transformed_convolution_r[index + (0*displ)] + transformed_convolution_r[index + (4*displ)] + transformed_convolution_r[index + (8*displ)]; 
        semi_res[1] = transformed_convolution_r[index + (1*displ)] + transformed_convolution_r[index + (5*displ)] + transformed_convolution_r[index + (9*displ)]; 
        semi_res[2] = transformed_convolution_r[index + (2*displ)] + transformed_convolution_r[index + (6*displ)] + transformed_convolution_r[index + (10*displ)]; 
        semi_res[3] = transformed_convolution_r[index + (3*displ)] + transformed_convolution_r[index + (7*displ)] + transformed_convolution_r[index + (11*displ)]; 
    
        semi_res[4] = transformed_convolution_r[index + (4*displ)] - transformed_convolution_r[index + (8*displ)] - transformed_convolution_r[index + (12*displ)];
        semi_res[5] = transformed_convolution_r[index + (5*displ)] - transformed_convolution_r[index + (9*displ)] - transformed_convolution_r[index + (13*displ)];
        semi_res[6] = transformed_convolution_r[index + (6*displ)] - transformed_convolution_r[index + (10*displ)] - transformed_convolution_r[index + (14*displ)];
        semi_res[7] = transformed_convolution_r[index + (7*displ)] - transformed_convolution_r[index + (11*displ)] - transformed_convolution_r[index + (15*displ)];


        semi_res[0] =  semi_res[0] + semi_res[1] + semi_res[2];
        semi_res[1] =  semi_res[1] - semi_res[2] - semi_res[3];
        semi_res[4] =  semi_res[4] + semi_res[5] + semi_res[6];
        semi_res[5] =  semi_res[5] - semi_res[6] - semi_res[7];
            
        index =  n_res;  
                   
                            
        res[ (k) + (px*K) + (py*(W-2)*K) + index] = semi_res[0]; 
        res[ (k) + ((px+1)*K) + (py*(W-2)*K) + index] = semi_res[1];
        res[ (k) + (px*K) + ((py+1)*(W-2)*K) + index] =  semi_res[4];
        res[ (k) + ((px+1)*K) + ((py+1)*(W-2)*K) + index] = semi_res[5];
                        
                   
    }  
}