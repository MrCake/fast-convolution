
extern inline void sub_filter_transformGEMMNHWC(float *filter, float*filter_transform, int C, int K)
{
    int R,S, out_R, out_S;
    R = S = 3;
    out_R = out_S = 4;
        
    float semi_filter_transform[12];
    int kernel_index, channel_index;
 

    int kernel_v = K/4;

    float32x4_t input[16];
    float32x4_t semi_filter_transform_v[12];
    float32x4_t half = vmovq_n_f32(0.5);


    for(int c=0; c< C; c++)
        for(int k=0; k< (kernel_v*4); k+=4)
        {
            input[0] = vld1q_f32(&filterCRSK(k,c,0,0, S,R,K));
            input[1] = vld1q_f32(&filterCRSK(k,c,0,1, S,R,K));
            input[2] = vld1q_f32(&filterCRSK(k,c,0,2, S,R,K));
        
            input[3] = vld1q_f32(&filterCRSK(k,c,1,0, S,R,K));
            input[4] = vld1q_f32(&filterCRSK(k,c,1,1, S,R,K));
            input[5] = vld1q_f32(&filterCRSK(k,c,1,2, S,R,K));
        
            input[6] = vld1q_f32(&filterCRSK(k,c,2,0, S,R,K));
            input[7] = vld1q_f32(&filterCRSK(k,c,2,1, S,R,K));     
            input[8] = vld1q_f32(&filterCRSK(k,c,2,2, S,R,K));
               

        
            //SEMI FILTER TRANSFORM (W*w)
            //fil=0,col=0-2
            semi_filter_transform_v[0] = input[0]; 
            semi_filter_transform_v[1] = input[1]; 
            semi_filter_transform_v[2] = input[2];
            //fil=3,col=0-2
            semi_filter_transform_v[9] = input[6];
            semi_filter_transform_v[10] = input[7];
            semi_filter_transform_v[11] = input[8];


            //fil=1,col=0-2 
            semi_filter_transform_v[3] = vmulq_f32( vaddq_f32( vaddq_f32(input[0], input[3]), input[6] ),  half); 
            semi_filter_transform_v[4] = vmulq_f32( vaddq_f32( vaddq_f32(input[1], input[4]), input[7] ),  half);
            semi_filter_transform_v[5] = vmulq_f32(vaddq_f32( vaddq_f32(input[2], input[5]), input[8] ), half);
            //fil=2,col=0-2 
            semi_filter_transform_v[6] = vmulq_f32( vaddq_f32( vsubq_f32(input[0], input[3]), input[6] ), half);
            semi_filter_transform_v[7] = vmulq_f32( vaddq_f32( vsubq_f32(input[1], input[4]), input[7] ), half);
            semi_filter_transform_v[8] = vmulq_f32( vaddq_f32( vsubq_f32(input[2], input[5]), input[8] ), half);
            


            //FILTER TRANSFORM (W*w*W'  4x4 transform)
            channel_index = c*(out_R*out_S*K);

            //r=0-3,s=0
            vst1q_f32(filter_transform + (k + (0*K) + (0*(out_S*K)) + channel_index), semi_filter_transform_v[0]);
            vst1q_f32(filter_transform + (k + (0*K) + (1*(out_S*K)) + channel_index), semi_filter_transform_v[3]);
            vst1q_f32(filter_transform + (k + (0*K) + (2*(out_S*K)) + channel_index), semi_filter_transform_v[6]);
            vst1q_f32(filter_transform + (k + (0*K) + (3*(out_S*K)) + channel_index), semi_filter_transform_v[9]);
            //r=0-3,s=3
            vst1q_f32(filter_transform + (k + (3*K) + (0*(out_S*K)) + channel_index), semi_filter_transform_v[2]);
            vst1q_f32(filter_transform + (k + (3*K) + (1*(out_S*K)) + channel_index), semi_filter_transform_v[5]);
            vst1q_f32(filter_transform + (k + (3*K) + (2*(out_S*K)) + channel_index), semi_filter_transform_v[8]);
            vst1q_f32(filter_transform + (k + (3*K) + (3*(out_S*K)) + channel_index), semi_filter_transform_v[11]);


            //r=0-3,s=1
            vst1q_f32(filter_transform + (k + (1*K) + (0*(out_S*K)) + channel_index), 
                vmulq_f32( vaddq_f32( vaddq_f32(semi_filter_transform_v[0], semi_filter_transform_v[1]), semi_filter_transform_v[2] ) , half) );
            vst1q_f32(filter_transform + (k + (1*K) + (1*(out_S*K)) + channel_index), 
                vmulq_f32( vaddq_f32( vaddq_f32(semi_filter_transform_v[3], semi_filter_transform_v[4]), semi_filter_transform_v[5] ), half) );
            vst1q_f32(filter_transform + (k + (1*K) + (2*(out_S*K)) + channel_index), 
                vmulq_f32( vaddq_f32( vaddq_f32(semi_filter_transform_v[6], semi_filter_transform_v[7]), semi_filter_transform_v[8] ), half) );
            vst1q_f32(filter_transform + (k + (1*K) + (3*(out_S*K)) + channel_index), 
                vmulq_f32( vaddq_f32( vaddq_f32(semi_filter_transform_v[9], semi_filter_transform_v[10]), semi_filter_transform_v[11] ), half) );

            //r=0-3,s=2
            vst1q_f32(filter_transform + (k + (2*K) + (0*(out_S*K)) + channel_index), 
                vmulq_f32( vaddq_f32( vsubq_f32(semi_filter_transform_v[0], semi_filter_transform_v[1]), semi_filter_transform_v[2] ), half) );
            vst1q_f32(filter_transform + (k + (2*K) + (1*(out_S*K)) + channel_index), 
                vmulq_f32( vaddq_f32( vsubq_f32(semi_filter_transform_v[3], semi_filter_transform_v[4]), semi_filter_transform_v[5] ), half ) );
            vst1q_f32(filter_transform + (k + (2*K) + (2*(out_S*K)) + channel_index), 
                vmulq_f32( vaddq_f32( vsubq_f32(semi_filter_transform_v[6], semi_filter_transform_v[7]), semi_filter_transform_v[8] ), half ) );
            vst1q_f32(filter_transform + (k + (2*K) + (3*(out_S*K)) + channel_index), 
                vmulq_f32( vaddq_f32( vsubq_f32(semi_filter_transform_v[9], semi_filter_transform_v[10]), semi_filter_transform_v[11] ), half ) );
         
        }
    
  

    for(int c=0 ; c<C; c++)
        for(int k=(kernel_v*4); k<K; k++)
        {
        
            //SEMI FILTER TRANSFORM (W*w)
            //fil=0,col=0-2
            semi_filter_transform[0] = filterCRSK(k,c,0,0, S,R,K); 
            semi_filter_transform[1] = filterCRSK(k,c,0,1, S,R,K); 
            semi_filter_transform[2] = filterCRSK(k,c,0,2, S,R,K); 
            //fil=1,col=0-2 
            semi_filter_transform[3] = ((filterCRSK(k,c,0,0, S,R,K) ) + (filterCRSK(k,c,1,0, S,R,K) ) + (filterCRSK(k,c,2,0, S,R,K) ))*0.5;
            semi_filter_transform[4] = ((filterCRSK(k,c,0,1, S,R,K) ) + (filterCRSK(k,c,1,1, S,R,K) ) + (filterCRSK(k,c,2,1, S,R,K) ))*0.5;
            semi_filter_transform[5] = ((filterCRSK(k,c,0,2, S,R,K) ) + (filterCRSK(k,c,1,2, S,R,K) ) + (filterCRSK(k,c,2,2, S,R,K) ))*0.5;
            //fil=2,col=0-2 
            semi_filter_transform[6] = ((filterCRSK(k,c,0,0, S,R,K) ) - (filterCRSK(k,c,1,0, S,R,K) ) + (filterCRSK(k,c,2,0, S,R,K) ))*0.5;
            semi_filter_transform[7] = ((filterCRSK(k,c,0,1, S,R,K) ) - (filterCRSK(k,c,1,1, S,R,K) ) + (filterCRSK(k,c,2,1, S,R,K) ))*0.5;
            semi_filter_transform[8] = ((filterCRSK(k,c,0,2, S,R,K) ) - (filterCRSK(k,c,1,2, S,R,K) ) + (filterCRSK(k,c,2,2, S,R,K) ))*0.5;
            //fil=3,col=0-2
            semi_filter_transform[9] =  filterCRSK(k,c,2,0, S,R,K);
            semi_filter_transform[10] = filterCRSK(k,c,2,1, S,R,K);
            semi_filter_transform[11] = filterCRSK(k,c,2,2, S,R,K);

            // print_matrix(semi_filter_transform, 4, 3, 1, 1);


            //FILTER TRANSFORM (W*w*W'  4x4 transform)
            channel_index = c*(out_R*out_S*K);
            //r=0,s=0
            filter_transform[k + (0*K) + (0*(out_S*K)) + channel_index] = semi_filter_transform[0];                
            //r=1,s=0
            filter_transform[k + (0*K) + (1*(out_S*K)) + channel_index] = semi_filter_transform[3];
            //r=2,s=0
            filter_transform[k + (0*K) + (2*(out_S*K)) + channel_index] = semi_filter_transform[6];
            //r=3,s=0
            filter_transform[k + (0*K) + (3*(out_S*K)) + channel_index] = semi_filter_transform[9];

            //r=0,s=1
            filter_transform[k + (1*K) + (0*(out_S*K)) + channel_index] = ((semi_filter_transform[0] ) + (semi_filter_transform[1] ) + (semi_filter_transform[2] ))*0.5;
            //r=1,s=1
            filter_transform[k + (1*K) + (1*(out_S*K)) + channel_index] = ((semi_filter_transform[3] ) + (semi_filter_transform[4] ) + (semi_filter_transform[5] ))*0.5;
            //r=2,s=1
            filter_transform[k + (1*K) + (2*(out_S*K)) + channel_index] = ((semi_filter_transform[6] ) + (semi_filter_transform[7] ) + (semi_filter_transform[8] ))*0.5;
            //r=3,s=1
            filter_transform[k + (1*K) + (3*(out_S*K)) + channel_index] = ((semi_filter_transform[9] ) + (semi_filter_transform[10] ) + (semi_filter_transform[11] ))*0.5;
            
            //r=0,s=2
            filter_transform[k + (2*K) + (0*(out_S*K)) + channel_index] = ((semi_filter_transform[0] ) - (semi_filter_transform[1] ) + (semi_filter_transform[2] ))*0.5;
            //r=1,s=2
            filter_transform[k + (2*K) + (1*(out_S*K)) + channel_index] = ((semi_filter_transform[3] ) - (semi_filter_transform[4] ) + (semi_filter_transform[5] ))*0.5;
            //r=2,s=2
            filter_transform[k + (2*K) + (2*(out_S*K)) + channel_index] = ((semi_filter_transform[6] ) - (semi_filter_transform[7] ) + (semi_filter_transform[8] ))*0.5;
            //r=3.s=2
            filter_transform[k + (2*K) + (3*(out_S*K)) + channel_index] = ((semi_filter_transform[9] ) - (semi_filter_transform[10] ) + (semi_filter_transform[11] ))*0.5;
            
            //r=0,s=3
            filter_transform[k + (3*K) + (0*(out_S*K)) + channel_index] = semi_filter_transform[2];
            //r=1,s=3
            filter_transform[k + (3*K) + (1*(out_S*K)) + channel_index] = semi_filter_transform[5];
            //r=2,s=3
            filter_transform[k + (3*K) + (2*(out_S*K)) + channel_index] = semi_filter_transform[8];
            //r=3,s=3
            filter_transform[k + (3*K) + (3*(out_S*K)) + channel_index] = semi_filter_transform[11];
        }
}