
extern inline void sub_image_transform_woddGEMMNHWC(float * image, float * image_transform, int n, int N, int C, int H, int W)
{
    int block_index = 0;
    int total_blocks = ((((W-2)/2)+1) * ((H-2)/2));

   

    float semi_image_transform[16];
    float semi_res[16];

    float32x4_t semi_res_v[8];
    float32x4_t semi_image_transform_v[16];

    
    int channel_v = C/4;

    double start_image, finish_image;

    float32x4_t input[16];
    for(int py=0; py < H-2; py+=2)
        for(int c=0; c < 4*channel_v; c+=4)
        {
            int px = 0;
            block_index = (px/2) + ((py/2)* (((W-2)/2)+1));

            input[0] = vld1q_f32(&imageNHWC(n,c,py,px,   W,H,C));
            input[1] = vld1q_f32(&imageNHWC(n,c,py,px+1,   W,H,C));
            input[2] = vld1q_f32(&imageNHWC(n,c,py,px+2,   W,H,C));
            input[3] = vld1q_f32(&imageNHWC(n,c,py,px+3,   W,H,C));

            input[4] = vld1q_f32(&imageNHWC(n,c,py+1,px,   W,H,C));
            input[5] = vld1q_f32(&imageNHWC(n,c,py+1,px+1,   W,H,C));
            input[6] = vld1q_f32(&imageNHWC(n,c,py+1,px+2,   W,H,C));
            input[7] = vld1q_f32(&imageNHWC(n,c,py+1,px+3,   W,H,C));
            
            input[8] = vld1q_f32(&imageNHWC(n,c,py+2,px,   W,H,C));
            input[9] = vld1q_f32(&imageNHWC(n,c,py+2,px+1,   W,H,C));
            input[10] = vld1q_f32(&imageNHWC(n,c,py+2,px+2,   W,H,C));
            input[11] = vld1q_f32(&imageNHWC(n,c,py+2,px+3,   W,H,C));

            input[12] = vld1q_f32(&imageNHWC(n,c,py+3,px,   W,H,C));
            input[13] = vld1q_f32(&imageNHWC(n,c,py+3,px+1,   W,H,C));
            input[14] = vld1q_f32(&imageNHWC(n,c,py+3,px+2,   W,H,C));
            input[15] = vld1q_f32(&imageNHWC(n,c,py+3,px+3,   W,H,C));

            semi_image_transform_v[0] = vsubq_f32(input[0] , input[8]);
            semi_image_transform_v[1] = vsubq_f32(input[1] , input[9]);
            semi_image_transform_v[2] = vsubq_f32(input[2] , input[10]);
            semi_image_transform_v[3] = vsubq_f32(input[3] , input[11]);

            semi_image_transform_v[4] = vaddq_f32(input[4], input[8]);
            semi_image_transform_v[5] = vaddq_f32(input[5], input[9]);
            semi_image_transform_v[6] = vaddq_f32(input[6], input[10]);
            semi_image_transform_v[7] = vaddq_f32(input[7], input[11]);

            semi_image_transform_v[8] = vsubq_f32(input[8], input[4]);
            semi_image_transform_v[9] = vsubq_f32(input[9], input[5]);
            semi_image_transform_v[10] = vsubq_f32(input[10], input[6]);
            semi_image_transform_v[11] = vsubq_f32(input[11], input[7]);

            semi_image_transform_v[12] = vsubq_f32(input[4], input[12]);
            semi_image_transform_v[13] = vsubq_f32(input[5], input[13]);
            semi_image_transform_v[14] = vsubq_f32(input[6], input[14]);
            semi_image_transform_v[15] = vsubq_f32(input[7], input[15]);
            
            //IMAGE TRANSFORM
            //fil=0-3,col=0
            vst1q_f32( (image_transform + ( c + (block_index*C) + (0*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[0], semi_image_transform_v[2])); 
            vst1q_f32( (image_transform + ( c + (block_index*C) + (4*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[4], semi_image_transform_v[6])); 
            vst1q_f32( (image_transform + ( c + (block_index*C) + (8*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[8], semi_image_transform_v[10])); 
            vst1q_f32( (image_transform + ( c + (block_index*C) + (12*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[12], semi_image_transform_v[14]));

            //fil=0-3,col=1
            vst1q_f32( (image_transform + ( c + (block_index*C) + (1*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[1], semi_image_transform_v[2]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (5*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[5], semi_image_transform_v[6]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (9*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[9], semi_image_transform_v[10]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (13*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[13], semi_image_transform_v[14]));  

            //fil=0-3,col=2
            vst1q_f32( (image_transform + ( c + (block_index*C) + (2*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[2], semi_image_transform_v[1]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (6*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[6], semi_image_transform_v[5]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (10*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[10], semi_image_transform_v[9]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (14*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[14], semi_image_transform_v[13]));  
            

            //fil=0-3,col=3
            vst1q_f32( (image_transform + ( c + (block_index*C) + (3*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[1], semi_image_transform_v[3]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (7*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[5], semi_image_transform_v[7]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (11*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[9], semi_image_transform_v[11]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (15*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[13], semi_image_transform_v[15]));

            semi_image_transform_v[0] = semi_image_transform_v[2];
            semi_image_transform_v[1] = semi_image_transform_v[3];

            semi_image_transform_v[4] = semi_image_transform_v[6];
            semi_image_transform_v[5] = semi_image_transform_v[7];

            semi_image_transform_v[8] = semi_image_transform_v[10];
            semi_image_transform_v[9] = semi_image_transform_v[11];

            semi_image_transform_v[12] = semi_image_transform_v[14];
            semi_image_transform_v[13] = semi_image_transform_v[15];

            for(int px=2; px < W-4; px+=2)
            {
                  block_index = (px/2) + ((py/2)* (((W-2)/2)+1));

              
                input[2] = vld1q_f32(&imageNHWC(n,c,py,px+2,   W,H,C));
                input[3] = vld1q_f32(&imageNHWC(n,c,py,px+3,   W,H,C));
                input[6] = vld1q_f32(&imageNHWC(n,c,py+1,px+2,   W,H,C));
                input[7] = vld1q_f32(&imageNHWC(n,c,py+1,px+3,   W,H,C));
                input[10] = vld1q_f32(&imageNHWC(n,c,py+2,px+2,   W,H,C));
                input[11] = vld1q_f32(&imageNHWC(n,c,py+2,px+3,   W,H,C));
                input[14] = vld1q_f32(&imageNHWC(n,c,py+3,px+2,   W,H,C));
                input[15] = vld1q_f32(&imageNHWC(n,c,py+3,px+3,   W,H,C));



                semi_image_transform_v[2] = vsubq_f32(input[2] , input[10]);
                semi_image_transform_v[3] = vsubq_f32(input[3] , input[11]);
                semi_image_transform_v[6] = vaddq_f32(input[6], input[10]);
                semi_image_transform_v[7] = vaddq_f32(input[7], input[11]);
                semi_image_transform_v[10] = vsubq_f32(input[10], input[6]);
                semi_image_transform_v[11] = vsubq_f32(input[11], input[7]);
                semi_image_transform_v[14] = vsubq_f32(input[6], input[14]);
                semi_image_transform_v[15] = vsubq_f32(input[7], input[15]);


                //IMAGE TRANSFORM
                //fil=0-3,col=0
                vst1q_f32( (image_transform + ( c + (block_index*C) + (0*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[0], semi_image_transform_v[2])); 
                vst1q_f32( (image_transform + ( c + (block_index*C) + (4*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[4], semi_image_transform_v[6])); 
                vst1q_f32( (image_transform + ( c + (block_index*C) + (8*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[8], semi_image_transform_v[10])); 
                vst1q_f32( (image_transform + ( c + (block_index*C) + (12*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[12], semi_image_transform_v[14]));

                //fil=0-3,col=1
                vst1q_f32( (image_transform + ( c + (block_index*C) + (1*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[1], semi_image_transform_v[2]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (5*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[5], semi_image_transform_v[6]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (9*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[9], semi_image_transform_v[10]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (13*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[13], semi_image_transform_v[14]));  

                //fil=0-3,col=2
                vst1q_f32( (image_transform + ( c + (block_index*C) + (2*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[2], semi_image_transform_v[1]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (6*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[6], semi_image_transform_v[5]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (10*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[10], semi_image_transform_v[9]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (14*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[14], semi_image_transform_v[13]));  
                

                //fil=0-3,col=3
                vst1q_f32( (image_transform + ( c + (block_index*C) + (3*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[1], semi_image_transform_v[3]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (7*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[5], semi_image_transform_v[7]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (11*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[9], semi_image_transform_v[11]));
                vst1q_f32( (image_transform + ( c + (block_index*C) + (15*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[13], semi_image_transform_v[15]));

                semi_image_transform_v[0] = semi_image_transform_v[2];
                semi_image_transform_v[1] = semi_image_transform_v[3];

                semi_image_transform_v[4] = semi_image_transform_v[6];
                semi_image_transform_v[5] = semi_image_transform_v[7];

                semi_image_transform_v[8] = semi_image_transform_v[10];
                semi_image_transform_v[9] = semi_image_transform_v[11];

                semi_image_transform_v[12] = semi_image_transform_v[14];
                semi_image_transform_v[13] = semi_image_transform_v[15];

            }

            px = W-4;
            block_index = ((px/2)+1) + ((py/2)* (((W-2)/2)+1));

            
            input[0] = vld1q_f32(&imageNHWC(n,c,py,px,   W,H,C));
            input[1] = vld1q_f32(&imageNHWC(n,c,py,px+1,   W,H,C));
            input[2] = vld1q_f32(&imageNHWC(n,c,py,px+2,   W,H,C));
            input[3] = vld1q_f32(&imageNHWC(n,c,py,px+3,   W,H,C));

            input[4] = vld1q_f32(&imageNHWC(n,c,py+1,px,   W,H,C));
            input[5] = vld1q_f32(&imageNHWC(n,c,py+1,px+1,   W,H,C));
            input[6] = vld1q_f32(&imageNHWC(n,c,py+1,px+2,   W,H,C));
            input[7] = vld1q_f32(&imageNHWC(n,c,py+1,px+3,   W,H,C));
            
            input[8] = vld1q_f32(&imageNHWC(n,c,py+2,px,   W,H,C));
            input[9] = vld1q_f32(&imageNHWC(n,c,py+2,px+1,   W,H,C));
            input[10] = vld1q_f32(&imageNHWC(n,c,py+2,px+2,   W,H,C));
            input[11] = vld1q_f32(&imageNHWC(n,c,py+2,px+3,   W,H,C));

            input[12] = vld1q_f32(&imageNHWC(n,c,py+3,px,   W,H,C));
            input[13] = vld1q_f32(&imageNHWC(n,c,py+3,px+1,   W,H,C));
            input[14] = vld1q_f32(&imageNHWC(n,c,py+3,px+2,   W,H,C));
            input[15] = vld1q_f32(&imageNHWC(n,c,py+3,px+3,   W,H,C));

            semi_image_transform_v[0] = vsubq_f32(input[0] , input[8]);
            semi_image_transform_v[1] = vsubq_f32(input[1] , input[9]);
            semi_image_transform_v[2] = vsubq_f32(input[2] , input[10]);
            semi_image_transform_v[3] = vsubq_f32(input[3] , input[11]);

            semi_image_transform_v[4] = vaddq_f32(input[4], input[8]);
            semi_image_transform_v[5] = vaddq_f32(input[5], input[9]);
            semi_image_transform_v[6] = vaddq_f32(input[6], input[10]);
            semi_image_transform_v[7] = vaddq_f32(input[7], input[11]);

            semi_image_transform_v[8] = vsubq_f32(input[8], input[4]);
            semi_image_transform_v[9] = vsubq_f32(input[9], input[5]);
            semi_image_transform_v[10] = vsubq_f32(input[10], input[6]);
            semi_image_transform_v[11] = vsubq_f32(input[11], input[7]);

            semi_image_transform_v[12] = vsubq_f32(input[4], input[12]);
            semi_image_transform_v[13] = vsubq_f32(input[5], input[13]);
            semi_image_transform_v[14] = vsubq_f32(input[6], input[14]);
            semi_image_transform_v[15] = vsubq_f32(input[7], input[15]);


            //IMAGE TRANSFORM
            //fil=0-3,col=0
            vst1q_f32( (image_transform + ( c + (block_index*C) + (0*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[0], semi_image_transform_v[2])); 
            vst1q_f32( (image_transform + ( c + (block_index*C) + (4*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[4], semi_image_transform_v[6])); 
            vst1q_f32( (image_transform + ( c + (block_index*C) + (8*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[8], semi_image_transform_v[10])); 
            vst1q_f32( (image_transform + ( c + (block_index*C) + (12*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[12], semi_image_transform_v[14]));

            //fil=0-3,col=1
            vst1q_f32( (image_transform + ( c + (block_index*C) + (1*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[1], semi_image_transform_v[2]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (5*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[5], semi_image_transform_v[6]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (9*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[9], semi_image_transform_v[10]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (13*total_blocks*C) )) , vaddq_f32(semi_image_transform_v[13], semi_image_transform_v[14]));  

            //fil=0-3,col=2
            vst1q_f32( (image_transform + ( c + (block_index*C) + (2*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[2], semi_image_transform_v[1]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (6*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[6], semi_image_transform_v[5]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (10*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[10], semi_image_transform_v[9]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (14*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[14], semi_image_transform_v[13]));  
            

            //fil=0-3,col=3
            vst1q_f32( (image_transform + ( c + (block_index*C) + (3*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[1], semi_image_transform_v[3]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (7*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[5], semi_image_transform_v[7]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (11*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[9], semi_image_transform_v[11]));
            vst1q_f32( (image_transform + ( c + (block_index*C) + (15*total_blocks*C) )) , vsubq_f32(semi_image_transform_v[13], semi_image_transform_v[15]));

      }
     
      for(int py=0; py < H-2; py+=2)
          for(int c=4*channel_v; c < C; ++c)
          {
              int px = 0;
              block_index = (px/2) + ((py/2)* (((W-2)/2)+1));

              //fil=0,col=0-3
              semi_image_transform[0] = imageNHWC(n,c,py,px,   W,H,C) - imageNHWC(n,c,py+2,px,   W,H,C);
              semi_image_transform[1] = imageNHWC(n,c,py,px+1, W,H,C) - imageNHWC(n,c,py+2,px+1, W,H,C);                
              semi_image_transform[2] = imageNHWC(n,c,py,px+2, W,H,C) - imageNHWC(n,c,py+2,px+2, W,H,C);
              semi_image_transform[3] = imageNHWC(n,c,py,px+3, W,H,C) - imageNHWC(n,c,py+2,px+3, W,H,C);
              //fil=1,col=0-3
              semi_image_transform[4] = imageNHWC(n,c,py+1,px,   W,H,C) + imageNHWC(n,c,py+2,px,   W,H,C);
              semi_image_transform[5] = imageNHWC(n,c,py+1,px+1, W,H,C) + imageNHWC(n,c,py+2,px+1, W,H,C);
              semi_image_transform[6] = imageNHWC(n,c,py+1,px+2, W,H,C) + imageNHWC(n,c,py+2,px+2, W,H,C);
              semi_image_transform[7] = imageNHWC(n,c,py+1,px+3, W,H,C) + imageNHWC(n,c,py+2,px+3, W,H,C);
              //fil=2,col=0-3
              semi_image_transform[8] =  (- imageNHWC(n,c,py+1,px,   W,H,C)) + imageNHWC(n,c,py+2,px,   W,H,C); 
              semi_image_transform[9] =  (- imageNHWC(n,c,py+1,px+1, W,H,C)) + imageNHWC(n,c,py+2,px+1, W,H,C);
              semi_image_transform[10] = (- imageNHWC(n,c,py+1,px+2, W,H,C)) + imageNHWC(n,c,py+2,px+2, W,H,C);
              semi_image_transform[11] = (- imageNHWC(n,c,py+1,px+3, W,H,C)) + imageNHWC(n,c,py+2,px+3, W,H,C);
              //fil=3,col=0-3
              semi_image_transform[12] = imageNHWC(n,c,py+1,px,   W,H,C) - imageNHWC(n,c,py+3,px,   W,H,C);
              semi_image_transform[13] = imageNHWC(n,c,py+1,px+1, W,H,C) - imageNHWC(n,c,py+3,px+1, W,H,C);
              semi_image_transform[14] = imageNHWC(n,c,py+1,px+2, W,H,C) - imageNHWC(n,c,py+3,px+2, W,H,C);
              semi_image_transform[15] = imageNHWC(n,c,py+1,px+3, W,H,C) - imageNHWC(n,c,py+3,px+3, W,H,C);
              
              
              
              //IMAGE TRANSFORM
              //fil=0-3,col=0
              image_transform[c + (block_index*C) + (0*total_blocks*C) ] = semi_image_transform[0] - semi_image_transform[2]; 
              image_transform[c + (block_index*C) + (4*total_blocks*C) ] = semi_image_transform[4] - semi_image_transform[6];
              image_transform[c + (block_index*C) + (8*total_blocks*C) ] = semi_image_transform[8] - semi_image_transform[10];
              image_transform[c + (block_index*C) + (12*total_blocks*C) ] = semi_image_transform[12] - semi_image_transform[14];
              //fil=0-3,col=1
              image_transform[c + (block_index*C) + (1*total_blocks*C) ] = semi_image_transform[1] + semi_image_transform[2];
              image_transform[c + (block_index*C) + (5*total_blocks*C) ] = semi_image_transform[5] + semi_image_transform[6];
              image_transform[c + (block_index*C) + (9*total_blocks*C) ] = semi_image_transform[9] + semi_image_transform[10];
              image_transform[c + (block_index*C) + (13*total_blocks*C) ] = semi_image_transform[13] + semi_image_transform[14];
              //fil=0-3,col=2
              image_transform[c + (block_index*C) + (2*total_blocks*C) ] = ( - semi_image_transform[1] ) + semi_image_transform[2]; 
              image_transform[c + (block_index*C) + (6*total_blocks*C) ] = ( - semi_image_transform[5] ) + semi_image_transform[6];
              image_transform[c + (block_index*C) + (10*total_blocks*C) ] = ( - semi_image_transform[9] ) + semi_image_transform[10];
              image_transform[c + (block_index*C) + (14*total_blocks*C) ] = ( - semi_image_transform[13] ) + semi_image_transform[14];
              //fil=0-3,col=3
              image_transform[c + (block_index*C) + (3*total_blocks*C) ] = semi_image_transform[1] - semi_image_transform[3];
              image_transform[c + (block_index*C) + (7*total_blocks*C) ] = semi_image_transform[5] - semi_image_transform[7];
              image_transform[c + (block_index*C) + (11*total_blocks*C) ] = semi_image_transform[9] - semi_image_transform[11];
              image_transform[c + (block_index*C) + (15*total_blocks*C) ] = semi_image_transform[13] - semi_image_transform[15];

              semi_image_transform[0] = semi_image_transform[2];
              semi_image_transform[1] = semi_image_transform[3];

              semi_image_transform[4] = semi_image_transform[6];
              semi_image_transform[5] = semi_image_transform[7];

              semi_image_transform[8] = semi_image_transform[10];
              semi_image_transform[9] = semi_image_transform[11];

              semi_image_transform[12] = semi_image_transform[14];
              semi_image_transform[13] = semi_image_transform[15];

              for(int px=2; px < W-4; px+=2)
              {
                  block_index = (px/2) + ((py/2)* (((W-2)/2)+1));
  
                  semi_image_transform[2] = imageNHWC(n,c,py,px+2, W,H,C) - imageNHWC(n,c,py+2,px+2, W,H,C);
                  semi_image_transform[3] = imageNHWC(n,c,py,px+3, W,H,C) - imageNHWC(n,c,py+2,px+3, W,H,C);
                
                  semi_image_transform[6] = imageNHWC(n,c,py+1,px+2, W,H,C) + imageNHWC(n,c,py+2,px+2, W,H,C);
                  semi_image_transform[7] = imageNHWC(n,c,py+1,px+3, W,H,C) + imageNHWC(n,c,py+2,px+3, W,H,C);
                  
                  semi_image_transform[10] = (- imageNHWC(n,c,py+1,px+2, W,H,C)) + imageNHWC(n,c,py+2,px+2, W,H,C);
                  semi_image_transform[11] = (- imageNHWC(n,c,py+1,px+3, W,H,C)) + imageNHWC(n,c,py+2,px+3, W,H,C);
              
                  semi_image_transform[14] = imageNHWC(n,c,py+1,px+2, W,H,C) - imageNHWC(n,c,py+3,px+2, W,H,C);
                  semi_image_transform[15] = imageNHWC(n,c,py+1,px+3, W,H,C) - imageNHWC(n,c,py+3,px+3, W,H,C);
                  
                  //IMAGE TRANSFORM
                  //fil=0-3,col=0
                  image_transform[c + (block_index*C) + (0*total_blocks*C) ] = semi_image_transform[0] - semi_image_transform[2]; 
                  image_transform[c + (block_index*C) + (4*total_blocks*C) ] = semi_image_transform[4] - semi_image_transform[6];
                  image_transform[c + (block_index*C) + (8*total_blocks*C) ] = semi_image_transform[8] - semi_image_transform[10];
                  image_transform[c + (block_index*C) + (12*total_blocks*C) ] = semi_image_transform[12] - semi_image_transform[14];
                  //fil=0-3,col=1
                  image_transform[c + (block_index*C) + (1*total_blocks*C) ] = semi_image_transform[1] + semi_image_transform[2];
                  image_transform[c + (block_index*C) + (5*total_blocks*C) ] = semi_image_transform[5] + semi_image_transform[6];
                  image_transform[c + (block_index*C) + (9*total_blocks*C) ] = semi_image_transform[9] + semi_image_transform[10];
                  image_transform[c + (block_index*C) + (13*total_blocks*C) ] = semi_image_transform[13] + semi_image_transform[14];
                  //fil=0-3,col=2
                  image_transform[c + (block_index*C) + (2*total_blocks*C) ] = ( - semi_image_transform[1] ) + semi_image_transform[2]; 
                  image_transform[c + (block_index*C) + (6*total_blocks*C) ] = ( - semi_image_transform[5] ) + semi_image_transform[6];
                  image_transform[c + (block_index*C) + (10*total_blocks*C) ] = ( - semi_image_transform[9] ) + semi_image_transform[10];
                  image_transform[c + (block_index*C) + (14*total_blocks*C) ] = ( - semi_image_transform[13] ) + semi_image_transform[14];
                  //fil=0-3,col=3
                  image_transform[c + (block_index*C) + (3*total_blocks*C) ] = semi_image_transform[1] - semi_image_transform[3];
                  image_transform[c + (block_index*C) + (7*total_blocks*C) ] = semi_image_transform[5] - semi_image_transform[7];
                  image_transform[c + (block_index*C) + (11*total_blocks*C) ] = semi_image_transform[9] - semi_image_transform[11];
                  image_transform[c + (block_index*C) + (15*total_blocks*C) ] = semi_image_transform[13] - semi_image_transform[15];
                  

                  semi_image_transform[0] = semi_image_transform[2];
                  semi_image_transform[1] = semi_image_transform[3];

                  semi_image_transform[4] = semi_image_transform[6];
                  semi_image_transform[5] = semi_image_transform[7];

                  semi_image_transform[8] = semi_image_transform[10];
                  semi_image_transform[9] = semi_image_transform[11];

                  semi_image_transform[12] = semi_image_transform[14];
                  semi_image_transform[13] = semi_image_transform[15];

              }

            px = W-4;
            block_index = ((px/2)+1) + ((py/2)* (((W-2)/2)+1));

            
            //fil=0,col=0-3
            semi_image_transform[0] = imageNHWC(n,c,py,px,   W,H,C) - imageNHWC(n,c,py+2,px,   W,H,C);
            semi_image_transform[1] = imageNHWC(n,c,py,px+1, W,H,C) - imageNHWC(n,c,py+2,px+1, W,H,C);                
            semi_image_transform[2] = imageNHWC(n,c,py,px+2, W,H,C) - imageNHWC(n,c,py+2,px+2, W,H,C);
            semi_image_transform[3] = imageNHWC(n,c,py,px+3, W,H,C) - imageNHWC(n,c,py+2,px+3, W,H,C);
            //fil=1,col=0-3
            semi_image_transform[4] = imageNHWC(n,c,py+1,px,   W,H,C) + imageNHWC(n,c,py+2,px,   W,H,C);
            semi_image_transform[5] = imageNHWC(n,c,py+1,px+1, W,H,C) + imageNHWC(n,c,py+2,px+1, W,H,C);
            semi_image_transform[6] = imageNHWC(n,c,py+1,px+2, W,H,C) + imageNHWC(n,c,py+2,px+2, W,H,C);
            semi_image_transform[7] = imageNHWC(n,c,py+1,px+3, W,H,C) + imageNHWC(n,c,py+2,px+3, W,H,C);
            //fil=2,col=0-3
            semi_image_transform[8] =  (- imageNHWC(n,c,py+1,px,   W,H,C)) + imageNHWC(n,c,py+2,px,   W,H,C); 
            semi_image_transform[9] =  (- imageNHWC(n,c,py+1,px+1, W,H,C)) + imageNHWC(n,c,py+2,px+1, W,H,C);
            semi_image_transform[10] = (- imageNHWC(n,c,py+1,px+2, W,H,C)) + imageNHWC(n,c,py+2,px+2, W,H,C);
            semi_image_transform[11] = (- imageNHWC(n,c,py+1,px+3, W,H,C)) + imageNHWC(n,c,py+2,px+3, W,H,C);
            //fil=3,col=0-3
            semi_image_transform[12] = imageNHWC(n,c,py+1,px,   W,H,C) - imageNHWC(n,c,py+3,px,   W,H,C);
            semi_image_transform[13] = imageNHWC(n,c,py+1,px+1, W,H,C) - imageNHWC(n,c,py+3,px+1, W,H,C);
            semi_image_transform[14] = imageNHWC(n,c,py+1,px+2, W,H,C) - imageNHWC(n,c,py+3,px+2, W,H,C);
            semi_image_transform[15] = imageNHWC(n,c,py+1,px+3, W,H,C) - imageNHWC(n,c,py+3,px+3, W,H,C);
            

            //IMAGE TRANSFORM
            //fil=0-3,col=0
            image_transform[c + (block_index*C) + (0*total_blocks*C) ] = semi_image_transform[0] - semi_image_transform[2]; 
            image_transform[c + (block_index*C) + (4*total_blocks*C) ] = semi_image_transform[4] - semi_image_transform[6];
            image_transform[c + (block_index*C) + (8*total_blocks*C) ] = semi_image_transform[8] - semi_image_transform[10];
            image_transform[c + (block_index*C) + (12*total_blocks*C) ] = semi_image_transform[12] - semi_image_transform[14];
            //fil=0-3,col=1
            image_transform[c + (block_index*C) + (1*total_blocks*C) ] = semi_image_transform[1] + semi_image_transform[2];
            image_transform[c + (block_index*C) + (5*total_blocks*C) ] = semi_image_transform[5] + semi_image_transform[6];
            image_transform[c + (block_index*C) + (9*total_blocks*C) ] = semi_image_transform[9] + semi_image_transform[10];
            image_transform[c + (block_index*C) + (13*total_blocks*C) ] = semi_image_transform[13] + semi_image_transform[14];
            //fil=0-3,col=2
            image_transform[c + (block_index*C) + (2*total_blocks*C) ] = ( - semi_image_transform[1] ) + semi_image_transform[2]; 
            image_transform[c + (block_index*C) + (6*total_blocks*C) ] = ( - semi_image_transform[5] ) + semi_image_transform[6];
            image_transform[c + (block_index*C) + (10*total_blocks*C) ] = ( - semi_image_transform[9] ) + semi_image_transform[10];
            image_transform[c + (block_index*C) + (14*total_blocks*C) ] = ( - semi_image_transform[13] ) + semi_image_transform[14];
            //fil=0-3,col=3
            image_transform[c + (block_index*C) + (3*total_blocks*C) ] = semi_image_transform[1] - semi_image_transform[3];
            image_transform[c + (block_index*C) + (7*total_blocks*C) ] = semi_image_transform[5] - semi_image_transform[7];
            image_transform[c + (block_index*C) + (11*total_blocks*C) ] = semi_image_transform[9] - semi_image_transform[11];
            image_transform[c + (block_index*C) + (15*total_blocks*C) ] = semi_image_transform[13] - semi_image_transform[15];
            
          }

}