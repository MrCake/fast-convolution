#include"./WConv_3x3.h"

//NCHW
#define image(n, c, i, j,    w, h, nc) (image[ (j) + ((i)*(w)) + ((c)*(w*h)) + ((n)*(nc*h*w))])
//CKRS
#define filter(k, c, i, j,    w, h, nk) (filter[ (c*(nk*h*w)) + (k*(w*h)) + ((i)*(w)+(j))])


// NHWC
#define imageNHWC(n, c, i, j,    w, h, nc) (image[ ((n)*(nc*h*w)) + ((i)*(w*nc)) + ((j)*(nc)) + (c) ])

//CRSK
#define filterCRSK(k, c, i, j,    w, h, nk) (filter[ (k) + ((j)*nk) + ((i)*nk*w) + ((c)*nk*w*h) ]) 

#include"./OneBlock/WOneBlock_NCHW-CKRS_NKRS.h"
#include"./ConvGEMM/ConvGEMM_NCHW-CKRS_NKRS.h"
#include"./OneBlockNHWC/WOneBlock_NHWC-CRSK_NRSK.h"
#include"./ConvGEMMNHWC/ConvGEMM_NCHW-CKRS_NKRS.h"
#include"./ConvGEMMBulk/ConvGEMMBulk_NCHW-CKRS_NKRS.h"
#include"./ConvGEMMNHWCBulk/ConvGEMMBulk_NCHW-CKRS_NKRS.h"

int WConv_3x3(float *image, float *filter, float *res, int H, int W, int C, int K, int N, int version)
{
    int R, S;
    R = 3;
    S = 3;
    if( W < 4 || H < 4)
    {
        return 1;
    }

    if(version == 0)
    {
        WOneBlock_NCHW_CKRS_NKRS(image, filter, res, H, W, R, S, C, K, N);
    }else if(version == 1)
    {
        ConvGEMM_NCHW_CKRS_NKRS(image, filter, res, H, W, R, S, C, K, N);
    }else if(version == 2)
    {
        WOneBlock_NHWC_CRSK_NRSK(image, filter, res, H, W, R, S, C, K, N);
    }else if(version == 3)
    {
        ConvGEMM_NHWC_CRSK_NRSK(image, filter, res, H, W, R, S, C, K, N);
    }else if(version == 11)
    {
        if( (W%2 == 0) && (H%2 == 0)){
            ConvGEMMBulk_NCHW_CKRS_NKRS(image, filter, res, H, W, R, S, C, K, N);
        }else{return 3;}
    }else if(version == 33)
    {
        if( (W%2 == 0) && (H%2 == 0)){
            ConvGEMMBulk_NHWC_CRSK_NRSK(image, filter, res, H, W, R, S, C, K, N);
        }else{return 3;}
        
    }else
    {
        return 2;
    }

    return 0;
}