#include<stdlib.h>
#include<omp.h>
#include <arm_neon.h>
#define BLIS_DISABLE_BLAS_DEFS
#include<blis.h>

#include<stdio.h>

int WConv_3x3(float *image, float *filter, float *res, int H, int W, int C, int K, int N, int version);
