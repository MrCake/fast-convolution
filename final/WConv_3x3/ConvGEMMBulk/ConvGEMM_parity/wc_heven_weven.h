

extern inline void wc_heven_wevenBulk(float *image, float *filter_transform, float *res, int W, int H, int C, int K, int N)
{

    int total_blocks =  (((W-2)/2) * ((H-2)/2));

        float *image_transform = calloc(4*4*total_blocks*C * N, 4);
        float *transformed_convolution = (float *) calloc(K*total_blocks*16 * N, 4);
        
   
    #pragma omp parallel 
    {

        // Image tranform into Winograd domain
        sub_imageBulk_transform(image, image_transform, -1, N, C, H, W);

            

        inc_t rowsA = total_blocks*N;
        inc_t colsB = K;
        inc_t common = C;
     
        inc_t rsa, csa;
        inc_t rsb, csb;
        inc_t rsc, csc;

        rsc = colsB; csc = 1;
        rsa = common; csa = 1;
        rsb = colsB*16; csb = 1;   

        float alpha, beta;
        alpha = 1.0;
        beta = 0.0;     
        
        int A_index,B_index,C_index;

        #pragma omp for
        for(int i=0; i < 16; i++)
            {
                A_index = (C*total_blocks*N*i);
                B_index = i*K;
                C_index = (K*total_blocks*N*i);
                bli_sgemm(BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE, rowsA, colsB, common, &alpha, 
                        &image_transform[A_index], rsa, csa, 
                        &filter_transform[B_index], rsb, csb, 
                        &beta, 
                        &transformed_convolution[C_index], rsc, csc);
                        
            }
     

        // Final transform back to initial domain
        sub_finalBulk_transform(transformed_convolution, res, -1, N, K, H, W);
        
    }
   
    
    
    free(image_transform);
    free(transformed_convolution);
    

    return;
}