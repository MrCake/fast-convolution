
extern inline void sub_imageBulk_transform(float * image, float * image_transform, int n, int N, int C, int H, int W)
{
    int block_index, channel_index, despl;
    int total_blocks = (((W-2)/2) * ((H-2)/2));

    float buff[16];
    float32x4_t input[4];
    float32x4_t semi_image_transform[4];

    float32x4x2_t trans1, trans2;
    float32x4x2_t input2_1, input2_2;
    
    #pragma omp for collapse(2)
    for(int n=0; n < N; n++)
    for(int py=0; py < H-3; py+=2)
        for(int px=0; px < W-3; px+=2)
            for(int c=0; c < C; c++)
            {
                block_index = (px/2) + ((py/2)* ((W-2)/2));

                input[0] = vld1q_f32(&image(n,c,py,px,   W,H,C));
                input[1] = vld1q_f32(&image(n,c,py+1,px,   W,H,C));
                input[2] = vld1q_f32(&image(n,c,py+2,px,   W,H,C));
                input[3] = vld1q_f32(&image(n,c,py+3,px,   W,H,C));

                semi_image_transform[0] = vsubq_f32(input[0], input[2]);
                semi_image_transform[1] = vaddq_f32(input[1], input[2]);
                semi_image_transform[2] = vsubq_f32(input[2], input[1]);
                semi_image_transform[3] = vsubq_f32(input[1], input[3]);

                
                // Transpose 4x4
                trans1 = vuzpq_f32(semi_image_transform[0], semi_image_transform[2]);   
                trans2 = vuzpq_f32(semi_image_transform[1], semi_image_transform[3]);   
                input2_1 = vtrnq_f32(trans1.val[0], trans2.val[0]);
                input2_2 = vtrnq_f32(trans1.val[1], trans2.val[1]);


                float32x4x4_t buff_ex = { vsubq_f32(input2_1.val[0], input2_1.val[1]), 
                            vaddq_f32(input2_2.val[0], input2_1.val[1]), 
                            vsubq_f32(input2_1.val[1], input2_2.val[0]), 
                            vsubq_f32(input2_2.val[0], input2_2.val[1]) };

                vst4q_lane_f32(buff, buff_ex, 0);
                vst4q_lane_f32(&buff[4], buff_ex, 1);
                vst4q_lane_f32(&buff[8], buff_ex, 2);
                vst4q_lane_f32(&buff[12], buff_ex, 3);

                // print_matrix(buff, 4, 4, 1, 1);
                channel_index = c + (block_index*C);
                int n_indx = n * (total_blocks*C);
                despl = total_blocks*C * N;
                
                for(int zz =0 ; zz < 16; zz++)
                {
                        image_transform[ channel_index + n_indx + (zz*despl) ] = buff[zz]; 
                }
                
            }
}