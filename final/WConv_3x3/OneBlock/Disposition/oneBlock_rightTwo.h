

extern inline void sub_oneBlock_rightTwo(float *image, float *res, float *filter_transform, float32x4x4_t filter_transform_i, int px, int py, int k, int n, int W, int old_W, int H, int old_H, int C, int K, int N)
{
    int index;
    float semi_res[8];
    register float32x4_t input[4];
    register float32x4_t semi_image_transform[4];
    register float32x4x2_t trans1, trans2;
    register float32x4x2_t fintrans1, fintrans2;
    float32x4x4_t transformed_convolution;

    int block_index = (px/2) + ((py/2)* ((W-2)/2));

    // Image transform to Winograd domain
    
    // C*d
    input[0] = vld1q_f32(&image(n,0,py,px,   W,H,C));
    input[1] = vld1q_f32(&image(n,0,py+1,px,   W,H,C));
    input[2] = vld1q_f32(&image(n,0,py+2,px,   W,H,C));
    input[3] = vld1q_f32(&image(n,0,py+3,px,   W,H,C));

    semi_image_transform[0] = vsubq_f32(input[0], input[2]);
    semi_image_transform[1] = vaddq_f32(input[1], input[2]);
    semi_image_transform[2] = vsubq_f32(input[2], input[1]);
    semi_image_transform[3] = vsubq_f32(input[1], input[3]);


    // Transpose 4x4 matrix to finish image transform 
    trans1 = vuzpq_f32(semi_image_transform[0], semi_image_transform[2]);   // 0 and 2
    trans2 = vuzpq_f32(semi_image_transform[1], semi_image_transform[3]);   // 1 and 3

    fintrans1 = vtrnq_f32(trans1.val[0], trans2.val[0]);
    fintrans2 = vtrnq_f32(trans1.val[1], trans2.val[1]);


    // C*d*C'
    //Transpose back to initial state 4x4 matrix
    trans1 = vuzpq_f32(vsubq_f32(fintrans1.val[0], fintrans1.val[1])  , vsubq_f32(fintrans1.val[1], fintrans2.val[0]));   // 0 and 2
    trans2 = vuzpq_f32(vaddq_f32(fintrans2.val[0], fintrans1.val[1])  , vsubq_f32(fintrans2.val[0], fintrans2.val[1]));   // 1 and 3

    fintrans1 = vtrnq_f32(trans1.val[0], trans2.val[0]);
    fintrans2 = vtrnq_f32(trans1.val[1], trans2.val[1]);

    float32x4x4_t image_transform = { fintrans1.val[0], 
                                        fintrans2.val[0], 
                                        fintrans1.val[1], 
                                        fintrans2.val[1]};


    // Hadamaard product for channel 0
    for(int zz=0; zz<4; zz++)
    transformed_convolution.val[zz] =
                    vmulq_f32(image_transform.val[zz], filter_transform_i.val[zz]);

    

    
        for(int c=1; c < C; c++)
        {
        float32x4x4_t filter_transform_i = {    vld1q_f32(filter_transform+(16*c) + (16*C*k)),
                                vld1q_f32(filter_transform+(16*c) + (16*C*k) +4),
                                vld1q_f32(filter_transform+(16*c) + (16*C*k) +8),
                                vld1q_f32(filter_transform+(16*c) + (16*C*k) +12) };

        // Image transform to Winograd domain

        // C*d
        input[0] = vld1q_f32(&image(n,c,py,px,   W,H,C));
        input[1] = vld1q_f32(&image(n,c,py+1,px,   W,H,C));
        input[2] = vld1q_f32(&image(n,c,py+2,px,   W,H,C));
        input[3] = vld1q_f32(&image(n,c,py+3,px,   W,H,C));

        semi_image_transform[0] = vsubq_f32(input[0], input[2]);
        semi_image_transform[1] = vaddq_f32(input[1], input[2]);
        semi_image_transform[2] = vsubq_f32(input[2], input[1]);
        semi_image_transform[3] = vsubq_f32(input[1], input[3]);

        // Transpose 4x4 matrix to finish image transform 
        trans1 = vuzpq_f32(semi_image_transform[0], semi_image_transform[2]);   // 0 and 2
        trans2 = vuzpq_f32(semi_image_transform[1], semi_image_transform[3]);   // 1 and 3

        fintrans1 = vtrnq_f32(trans1.val[0], trans2.val[0]);
        fintrans2 = vtrnq_f32(trans1.val[1], trans2.val[1]);

        // C*d*C'
        //Transpose back to initial state 4x4 matrix
        trans1 = vuzpq_f32(vsubq_f32(fintrans1.val[0], fintrans1.val[1])  , vsubq_f32(fintrans1.val[1], fintrans2.val[0]));   // 0 and 2
        trans2 = vuzpq_f32(vaddq_f32(fintrans2.val[0], fintrans1.val[1])  , vsubq_f32(fintrans2.val[0], fintrans2.val[1]));   // 1 and 3

        fintrans1 = vtrnq_f32(trans1.val[0], trans2.val[0]);
        fintrans2 = vtrnq_f32(trans1.val[1], trans2.val[1]);

        float32x4x4_t image_transform = { fintrans1.val[0], 
                                        fintrans2.val[0], 
                                        fintrans1.val[1], 
                                        fintrans2.val[1]};


        // Hadamaard product and acumulation for (channel > 0)
        for(int zz=0; zz<4; zz++)
            transformed_convolution.val[zz] =
                            vfmaq_f32(transformed_convolution.val[zz] , image_transform.val[zz], filter_transform_i.val[zz]);

    }

    

    // Transform back to initial domain

    // A'* (transformed_convolution)
    float32x4x2_t semi_r = { vaddq_f32( vaddq_f32( transformed_convolution.val[0], transformed_convolution.val[1] ), transformed_convolution.val[2] ) ,
                            vsubq_f32( vsubq_f32( transformed_convolution.val[1], transformed_convolution.val[2] ), transformed_convolution.val[3] )
                            };
    

    vst1q_f32(semi_res, semi_r.val[0]);
    vst1q_f32(semi_res+4, semi_r.val[1]);
    
    index = (k*(H-2)*(W-2)) + (n*K*(H-2)*(W-2));
    
    //  A'* (transformed_convolution) * A
    res[ (px+1) + (py*(old_W)) + index] = semi_res[1] - semi_res[2] - semi_res[3];
    res[ (px+1) + ((py+1)*(old_W)) + index] = semi_res[5] - semi_res[6] - semi_res[7];

} 