#include"./Disposition/oneBlock.h"
#include"./Disposition/oneBlock_rightTwo.h"
#include"./Disposition/oneBlock_botTwo.h"
#include"./Disposition/oneBlock_botrightTwo.h"


extern inline void WOneBlock_NCHW_CKRS_NKRS(float* image, float* filter, float *res, int H, int W, int R, int S, int C, int K, int N)
{

    int old_H,old_W;
    int kernel_index, channel_index;
    int out_R, out_S;

    old_H = H-2;
    old_W = W-2;
    out_R = out_S = 4;

   float *filter_transform = (float *) calloc(4*4*C*K, 4);


    omp_set_nested(1);
    #pragma omp parallel if ((N+K) >= omp_get_num_threads())
    {    

    float semi_transform[16];
        
    
    #pragma omp for
    for(int c=0; c<C; c++)
        for(int k=0; k<K; k++)
        {
        
            //SEMI FILTER TRANSFORM (W*w)
            semi_transform[0] = filter(k,c,0,0, S,R,K); 
            semi_transform[1] = filter(k,c,0,1, S,R,K); 
            semi_transform[2] = filter(k,c,0,2, S,R,K); 
            semi_transform[3] = (filter(k,c,0,0, S,R,K) + filter(k,c,1,0, S,R,K)  + filter(k,c,2,0, S,R,K)) * 0.5;
            semi_transform[4] = (filter(k,c,0,1, S,R,K) + filter(k,c,1,1, S,R,K)  + filter(k,c,2,1, S,R,K)) * 0.5;
            semi_transform[5] = (filter(k,c,0,2, S,R,K) + filter(k,c,1,2, S,R,K)  + filter(k,c,2,2, S,R,K)) * 0.5;
            semi_transform[6] = (filter(k,c,0,0, S,R,K) - filter(k,c,1,0, S,R,K)  + filter(k,c,2,0, S,R,K)) * 0.5;
            semi_transform[7] = (filter(k,c,0,1, S,R,K) - filter(k,c,1,1, S,R,K)  + filter(k,c,2,1, S,R,K)) * 0.5;
            semi_transform[8] = (filter(k,c,0,2, S,R,K) - filter(k,c,1,2, S,R,K)  + filter(k,c,2,2, S,R,K)) * 0.5;
            semi_transform[9] =  filter(k,c,2,0, S,R,K);
            semi_transform[10] = filter(k,c,2,1, S,R,K);
            semi_transform[11] = filter(k,c,2,2, S,R,K);


            //FILTER TRANSFORM (W*w*W'  4x4 transform)
            channel_index = c*(out_R*out_S);
            kernel_index = k*(out_R*out_S*C);

            filter_transform[(0) + (0*(out_S)) + channel_index + kernel_index ] = semi_transform[0];                
            filter_transform[(0) + (1*(out_S)) + channel_index + kernel_index] = semi_transform[3];
            filter_transform[(0) + (2*(out_S)) + channel_index + kernel_index] = semi_transform[6];
            filter_transform[(0) + (3*(out_S)) + channel_index + kernel_index] = semi_transform[9];
            filter_transform[(1) + (0*(out_S)) + channel_index + kernel_index] = (semi_transform[0] + semi_transform[1]  + semi_transform[2]) * 0.5;
            filter_transform[(1) + (1*(out_S)) + channel_index + kernel_index] = (semi_transform[3] + semi_transform[4]  + semi_transform[5]) * 0.5;
            filter_transform[(1) + (2*(out_S)) + channel_index + kernel_index] = (semi_transform[6] + semi_transform[7]  + semi_transform[8]) * 0.5;
            filter_transform[(1) + (3*(out_S)) + channel_index + kernel_index] = (semi_transform[9] + semi_transform[10] + semi_transform[11]) * 0.5;
            filter_transform[(2) + (0*(out_S)) + channel_index + kernel_index] = (semi_transform[0] - semi_transform[1]  + semi_transform[2]) * 0.5;
            filter_transform[(2) + (1*(out_S)) + channel_index + kernel_index] = (semi_transform[3] - semi_transform[4]  + semi_transform[5]) * 0.5;
            filter_transform[(2) + (2*(out_S)) + channel_index + kernel_index] = (semi_transform[6] - semi_transform[7]  + semi_transform[8]) * 0.5;
            filter_transform[(2) + (3*(out_S)) + channel_index + kernel_index] = (semi_transform[9] - semi_transform[10] + semi_transform[11]) * 0.5;
            filter_transform[(3) + (0*(out_S)) + channel_index + kernel_index] = semi_transform[2];
            filter_transform[(3) + (1*(out_S)) + channel_index + kernel_index] = semi_transform[5];
            filter_transform[(3) + (2*(out_S)) + channel_index + kernel_index] = semi_transform[8];
            filter_transform[(3) + (3*(out_S)) + channel_index + kernel_index] = semi_transform[11];
        }

 
    
    

  
  
    if((W%2 == 0) && (H%2 == 0))
    {
        #pragma omp for collapse(2)
        for(int n=0; n < N; n++)   
            for(int k=0; k < K; k++)
            {
                float32x4x4_t filter_transform_i = {    vld1q_f32(filter_transform+(16*C*k)),
                                                        vld1q_f32(filter_transform+(16*C*k)+4),
                                                        vld1q_f32(filter_transform+(16*C*k)+8),
                                                        vld1q_f32(filter_transform+(16*C*k)+12) };
                #pragma omp parallel for if ((N+K) < omp_get_num_threads())
                for(int py=0; py < H-2; py+=2)
                    for(int px=0; px < W-2 ; px+=2)
                    {  

                        sub_oneBlock(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);
                                                
                    }
            }       
    }else if((W%2 == 0) || (H%2 == 0))
    {
        if(W%2 == 1)
        {
            #pragma omp for collapse(2)
            for(int n=0; n < N; n++)   
                for(int k=0; k < K; k++)
                {
                    float32x4x4_t filter_transform_i = {    vld1q_f32(filter_transform+(16*C*k)),
                                                            vld1q_f32(filter_transform+(16*C*k)+4),
                                                            vld1q_f32(filter_transform+(16*C*k)+8),
                                                            vld1q_f32(filter_transform+(16*C*k)+12) };
                    #pragma omp parallel for if ((N+K) < omp_get_num_threads())
                    for(int py=0; py < H-2; py+=2)
                    {
                        for(int px=0; px < W-4 ; px+=2)
                        {  
                            sub_oneBlock(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);                         
                        }
                        int px = W-4;
                        sub_oneBlock_rightTwo(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);
                    }
                }
        
        }else if(H%2 == 1)
        {
            #pragma omp for collapse(2)
            for(int n=0; n < N; n++)   
                for(int k=0; k < K; k++)
                {
                    float32x4x4_t filter_transform_i = {    vld1q_f32(filter_transform+(16*C*k)),
                                                            vld1q_f32(filter_transform+(16*C*k)+4),
                                                            vld1q_f32(filter_transform+(16*C*k)+8),
                                                            vld1q_f32(filter_transform+(16*C*k)+12) };
                    #pragma omp parallel for if ((N+K) < omp_get_num_threads())
                    for(int py=0; py < H-4; py+=2)
                        for(int px=0; px < W-2 ; px+=2)
                        {  
                            sub_oneBlock(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);                         
                        }
                    int py = H-4;
                    for(int px=0; px < W-2 ; px+=2)
                    {  
                        sub_oneBlock_botTwo(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);                         
                    }
                }
        }
    }else if((W%2 == 1) && (H%2 == 1))
    {
        #pragma omp for collapse(2)
        for(int n=0; n < N; n++)   
            for(int k=0; k < K; k++)
            {
                float32x4x4_t filter_transform_i = {    vld1q_f32(filter_transform+(16*C*k)),
                                                        vld1q_f32(filter_transform+(16*C*k)+4),
                                                        vld1q_f32(filter_transform+(16*C*k)+8),
                                                        vld1q_f32(filter_transform+(16*C*k)+12) };
                #pragma omp parallel for if ((N+K) < omp_get_num_threads())
                for(int py=0; py < H-4; py+=2)
                {
                    for(int px=0; px < W-4 ; px+=2)
                    {  
                        sub_oneBlock(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);                         
                    }
                    int px = W-4;
                    sub_oneBlock_rightTwo(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);
                }
                int py = H-4;
                for(int px=0; px < W-4 ; px+=2)
                {  
                    sub_oneBlock_botTwo(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);                         
                }
                int px = W-4;
                sub_oneBlock_botrightTwo(image, res, filter_transform, filter_transform_i, px, py, k, n, W, old_W, H, old_H, C, K, N);  
            }
    }


    }
    free(filter_transform);

        
    return;
}