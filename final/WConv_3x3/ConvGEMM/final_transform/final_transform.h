
extern inline void sub_final_transform(float * transformed_convolution, float * res, int n, int N, int K, int H, int W)
{

    int b, index, displ;
     
    int kernel_v = (K>>2)*4;
  
    
    int total_image = (W-2)*(H-2);
     
    
    
    float32x4_t input[16];
    register float32x4_t semi_res_v[8];
    float semi_res[16];

    int total_blocks = (((W-2)>>1) * ((H-2)>>1));
    // W=W-2;
    // H=H-2;


    int pos_vector[16];
    for(int i=0; i<16; i++)
        pos_vector[i] = i*K*total_blocks + (i*16);

    int n_res = (n*K*(H-2)*(W-2));

        for(int h=0; h < (H-2); h+=2)
            for(int w=0; w < (W-2); w+=2)
                for( int k=0; k < kernel_v; k+=4)
                    {
                        b = (w>>1)+(((h>>1))*((W-2)>>1));
                        index = k + (b * K);

                
                        input[0] = vld1q_f32(transformed_convolution + (index + pos_vector[0] ));
                        input[1] = vld1q_f32(transformed_convolution + (index + (pos_vector[1]) ));
                        input[2] = vld1q_f32(transformed_convolution + (index + (pos_vector[2]) ));
                        input[3] = vld1q_f32(transformed_convolution + (index + (pos_vector[3]) ));

                        input[4] = vld1q_f32(transformed_convolution + (index + (pos_vector[4]) ));
                        input[5] = vld1q_f32(transformed_convolution + (index + (pos_vector[5]) ));
                        input[6] = vld1q_f32(transformed_convolution + (index + (pos_vector[6]) ));
                        input[7] = vld1q_f32(transformed_convolution + (index + (pos_vector[7]) ));

                        input[8] = vld1q_f32(transformed_convolution + (index + (pos_vector[8]) ));
                        input[9] = vld1q_f32(transformed_convolution + (index + (pos_vector[9]) ));
                        input[10] = vld1q_f32(transformed_convolution + (index + (pos_vector[10]) ));
                        input[11] = vld1q_f32(transformed_convolution + (index + (pos_vector[11]) ));


                        input[12] = vld1q_f32(transformed_convolution + (index + (pos_vector[12]) ));
                        input[13] = vld1q_f32(transformed_convolution + (index + (pos_vector[13]) ));
                        input[14] = vld1q_f32(transformed_convolution + (index + (pos_vector[14]) ));
                        input[15] = vld1q_f32(transformed_convolution + (index + (pos_vector[15]) ));

                        //fil=0,col=0-3
                        
                        semi_res_v[0] = vaddq_f32( vaddq_f32(input[0], input[4]), input[8] );
                        semi_res_v[1] = vaddq_f32( vaddq_f32(input[1], input[5]), input[9] );                     
                        semi_res_v[2] = vaddq_f32( vaddq_f32(input[2], input[6]), input[10] );
                        semi_res_v[3] = vaddq_f32( vaddq_f32(input[3], input[7]), input[11] );
                        //fil=1,col=0-3



                        semi_res_v[4] = vsubq_f32( vsubq_f32(input[4], input[8]), input[12] );
                        semi_res_v[5] = vsubq_f32( vsubq_f32(input[5], input[9]), input[13] );                            
                        semi_res_v[6] = vsubq_f32( vsubq_f32(input[6], input[10]), input[14] );
                        semi_res_v[7] = vsubq_f32( vsubq_f32(input[7], input[11]), input[15] );
           


                        semi_res_v[0] = vaddq_f32( vaddq_f32(semi_res_v[0], semi_res_v[1]), semi_res_v[2]);
                        semi_res_v[1] = vsubq_f32( vsubq_f32(semi_res_v[1], semi_res_v[2]), semi_res_v[3]);
                        semi_res_v[4] = vaddq_f32( vaddq_f32(semi_res_v[4], semi_res_v[5]), semi_res_v[6]);
                        semi_res_v[5] = vsubq_f32( vsubq_f32(semi_res_v[5], semi_res_v[6]), semi_res_v[7]);


                         displ = (W-2);

                        for (int zz=0; zz < 4; ++zz)
                        {
                       
                            index = ((k+zz)*total_image + n_res);
                            res[ (w) + (h* displ ) + index] = vgetq_lane_f32(semi_res_v[0], zz);
                            res[ (w+1) + (h* displ ) + index] = vgetq_lane_f32(semi_res_v[1],zz);

                            res[ (w) + ((h+1)* displ ) + index] = vgetq_lane_f32(semi_res_v[4],zz);
                            res[ (w+1) + ((h+1)* displ) + index] = vgetq_lane_f32(semi_res_v[5],zz);
                        }    
                        
                    }
            
           
            for(int h=0; h < (H-2); h+=2)
            for(int w=0; w < (W-2); w+=2)
                for(int k= (kernel_v); k < K ; ++k)
                {
                        b = (w/2)+((h/2)*((W-2)/2));
                        index = k + (b * K);

                        semi_res[0] = transformed_convolution[index + pos_vector[0]] + transformed_convolution[index + pos_vector[4]] + transformed_convolution[index + pos_vector[8]]; 
                        semi_res[1] = transformed_convolution[index + pos_vector[1]] + transformed_convolution[index + pos_vector[5]] + transformed_convolution[index + pos_vector[9]]; 
                        semi_res[2] = transformed_convolution[index + pos_vector[2]] + transformed_convolution[index + pos_vector[6]] + transformed_convolution[index + pos_vector[10]]; 
                        semi_res[3] = transformed_convolution[index + pos_vector[3]] + transformed_convolution[index + pos_vector[7]] + transformed_convolution[index + pos_vector[11]]; 
                  
                        semi_res[4] = transformed_convolution[index + pos_vector[4]] - transformed_convolution[index + pos_vector[8]] - transformed_convolution[index + pos_vector[12]];
                        semi_res[5] = transformed_convolution[index + pos_vector[5]] - transformed_convolution[index + pos_vector[9]] - transformed_convolution[index + pos_vector[13]];
                        semi_res[6] = transformed_convolution[index + pos_vector[6]] - transformed_convolution[index + pos_vector[10]] - transformed_convolution[index + pos_vector[14]];
                        semi_res[7] = transformed_convolution[index + pos_vector[7]] - transformed_convolution[index + pos_vector[11]] - transformed_convolution[index + pos_vector[15]];

                        // print_matrix(semi_res, 2, 4, 1, 1);
                        index = (k*(total_image)) + (n*K*(total_image));
                        displ = (W-2);

                        res[ (w) + (h*displ) + index] = semi_res[0] + semi_res[1] + semi_res[2];
                        res[ (w+1) + (h*displ) + index] = semi_res[1] - semi_res[2] - semi_res[3];


                        res[ (w) + ((h+1)*displ) + index] = semi_res[4] + semi_res[5] + semi_res[6];
                        res[ (w+1) + ((h+1)*displ) + index] = semi_res[5] - semi_res[6] - semi_res[7];
                }
    
        
} 