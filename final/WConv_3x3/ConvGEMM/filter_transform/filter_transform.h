
extern inline void sub_filter_transform(float *filter, float*filter_transform, int C, int K)
{
    float semi_filter_transform[12];
    int channel_index, despl;
    int S,R, out_R, out_S;
    R = S = 3;
    out_R = out_S = 4;

    for(int c=0; c<C; c++)
      for(int k=0; k<K; k++)
      {
        //SEMI FILTER TRANSFORM (W*w)
        semi_filter_transform[0] = filter(k,c,0,0, S,R,K); 
        semi_filter_transform[1] = filter(k,c,0,1, S,R,K); 
        semi_filter_transform[2] = filter(k,c,0,2, S,R,K); 
    
        semi_filter_transform[3] = ((filter(k,c,0,0, S,R,K)) + (filter(k,c,1,0, S,R,K)) + (filter(k,c,2,0, S,R,K))) * 0.5;
        semi_filter_transform[4] = ((filter(k,c,0,1, S,R,K)) + (filter(k,c,1,1, S,R,K)) + (filter(k,c,2,1, S,R,K))) * 0.5;
        semi_filter_transform[5] = ((filter(k,c,0,2, S,R,K)) + (filter(k,c,1,2, S,R,K)) + (filter(k,c,2,2, S,R,K))) * 0.5;
    
        semi_filter_transform[6] = ((filter(k,c,0,0, S,R,K)) - (filter(k,c,1,0, S,R,K)) + (filter(k,c,2,0, S,R,K))) * 0.5;
        semi_filter_transform[7] = ((filter(k,c,0,1, S,R,K)) - (filter(k,c,1,1, S,R,K)) + (filter(k,c,2,1, S,R,K))) * 0.5;
        semi_filter_transform[8] = ((filter(k,c,0,2, S,R,K)) - (filter(k,c,1,2, S,R,K)) + (filter(k,c,2,2, S,R,K))) * 0.5;
    
        semi_filter_transform[9] =  filter(k,c,2,0, S,R,K);
        semi_filter_transform[10] = filter(k,c,2,1, S,R,K);
        semi_filter_transform[11] = filter(k,c,2,2, S,R,K);

        //FILTER TRANSFORM (W*w*W'  4x4 transform)
        channel_index = c*(out_R*out_S*K) + k;
        despl = out_S*K;

        filter_transform[ (0*K) + (0*despl) + channel_index] = semi_filter_transform[0];                
        filter_transform[ (0*K) + (1*despl) + channel_index] = semi_filter_transform[3];
        filter_transform[ (0*K) + (2*despl) + channel_index] = semi_filter_transform[6];
        filter_transform[ (0*K) + (3*despl) + channel_index] = semi_filter_transform[9];

        filter_transform[ (1*K) + (0*despl) + channel_index] = ((semi_filter_transform[0]) + (semi_filter_transform[1]) + (semi_filter_transform[2])) * 0.5;
        filter_transform[ (1*K) + (1*despl) + channel_index] = ((semi_filter_transform[3]) + (semi_filter_transform[4]) + (semi_filter_transform[5])) * 0.5;
        filter_transform[ (1*K) + (2*despl) + channel_index] = ((semi_filter_transform[6]) + (semi_filter_transform[7]) + (semi_filter_transform[8])) * 0.5;
        filter_transform[ (1*K) + (3*despl) + channel_index] = ((semi_filter_transform[9]) + (semi_filter_transform[10]) + (semi_filter_transform[11])) * 0.5;
        
        filter_transform[ (2*K) + (0*despl) + channel_index] = ((semi_filter_transform[0]) - (semi_filter_transform[1]) + (semi_filter_transform[2])) * 0.5;
        filter_transform[ (2*K) + (1*despl) + channel_index] = ((semi_filter_transform[3]) - (semi_filter_transform[4]) + (semi_filter_transform[5])) * 0.5;
        filter_transform[ (2*K) + (2*despl) + channel_index] = ((semi_filter_transform[6]) - (semi_filter_transform[7]) + (semi_filter_transform[8])) * 0.5;
        filter_transform[ (2*K) + (3*despl) + channel_index] = ((semi_filter_transform[9]) - (semi_filter_transform[10]) + (semi_filter_transform[11])) * 0.5;
        
        filter_transform[ (3*K) + (0*despl) + channel_index] = semi_filter_transform[2];
        filter_transform[ (3*K) + (1*despl) + channel_index] = semi_filter_transform[5];
        filter_transform[ (3*K) + (2*despl) + channel_index] = semi_filter_transform[8];
        filter_transform[ (3*K) + (3*despl) + channel_index] = semi_filter_transform[11];
      }
}