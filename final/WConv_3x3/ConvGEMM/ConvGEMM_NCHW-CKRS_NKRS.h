
#include"./filter_transform/filter_transform.h"


#include"./image_transform/image_transform.h"
#include"./image_transform/image_transform_wodd.h"
#include"./image_transform/image_transform_hodd.h"
#include"./image_transform/image_transform_hodd_wodd.h"

#include"./final_transform/final_transform.h"
#include"./final_transform/final_transform_wodd.h"
#include"./final_transform/final_transform_hodd.h"
#include"./final_transform/final_transform_hodd_wodd.h"


#include"./ConvGEMM_parity/wc_heven_weven.h"
#include"./ConvGEMM_parity/wc_heven_wodd.h"
#include"./ConvGEMM_parity/wc_hodd_weven.h"
#include"./ConvGEMM_parity/wc_hodd_wodd.h"

extern inline void ConvGEMM_NCHW_CKRS_NKRS(float* image, float* filter, float *res, int H, int W, int R, int S, int C, int K, int N)
{
    int old_H,old_W;
    old_H = H-2;
    old_W = W-2;
    


    int total_blocks = ((((W-2)/2)+1) * (((H-2)/2)+1));


    float *filter_transform = (float *) calloc(4*4*C*K, 4);    

  
    
  
    // Calculate Filter tranformation to winograd domain
    sub_filter_transform(filter, filter_transform, C, K);
    // if (N >= omp_get_num_threads())
    #pragma omp parallel 
    {
    if ((W%2 == 0) && (H%2 == 0))
    {
        wc_heven_weven(image,filter_transform, res, W, H, C, K, N);
    }else if((W%2 == 0) || (H%2 == 0))
    {
        if(W%2 == 1)
        {
             wc_heven_wodd(image,filter_transform, res, W, H, C, K, N);
        }else if(H%2 == 1)
        {
            wc_hodd_weven(image,filter_transform,res, W, H, C, K, N);
        }
    }else if ((W%2 == 1) && (H%2 == 1))
    {
        wc_hodd_wodd(image,filter_transform, res, W, H, C, K, N);
    }
    
    }
    
    free(filter_transform);
   
    return;
}