
#include"./filter_transform/filter_transform.h"

#include"./image_transform/image_transform.h"

#include"./final_transform/final_transform.h"

#include"./ConvGEMM_parity/wc_heven_weven.h"


extern inline void ConvGEMMBulk_NHWC_CRSK_NRSK(float* image, float* filter, float *res, int H, int W, int R, int S, int C, int K, int N)
{
    int old_H,old_W;
    old_H = H-2;
    old_W = W-2;
    

    int total_blocks = ((((W-2)/2)+1) * (((H-2)/2)+1));   
    
    float *filter_transform = (float *) calloc(4*4*C*K, 4);
   


    // Calculate Filter tranformation to winograd domain
    sub_filterBulk_transformGEMMNHWC(filter, filter_transform, C, K);

 
    if ((W%2 == 0) && (H%2 == 0))
    {
        wc_heven_wevenGEMMNHWCBulk(image,filter_transform, res, W, H, C, K, N);
    }
 
    
    free(filter_transform);

  
   
    return;
}