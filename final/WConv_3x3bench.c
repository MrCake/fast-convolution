#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<cblas.h>
#include <arm_neon.h>

#include "./normConv/normConv.h"
#include "./normConv/normConvNHWC.h"
#include "./WConv_3x3/WConv_3x3.h"

#define image_rNHWC(n, c, i, j,    w, h, nc) (image_r[ ((n)*(nc*h*w)) + ((i)*(w*nc)) + ((j)*(nc)) + (c) ])

float* gen_image(int n, float max_num);
void print_matrix(float* image, int h, int w, int k, int n);
int compare(float* image, float* res, int n);
float *zero_pad(float *image_r, int H, int W, int C, int N );
float *zero_padNHWC(float *image_r, int H, int W, int C, int N );
double bench(int N, int C, int K, int H, int W, int version);
double fops_calc(int N, int C, int K, int H, int W);
void gen_bench( int N, int C, int K, int H, int W, int version );
void bench_0();
void bench_1();
void bench_2();
void bench_3();
void bench_4();
void bench_5();
void bench_6();

#define image_rNHWC(n, c, i, j,    w, h, nc) (image_r[ ((n)*(nc*h*w)) + ((i)*(w*nc)) + ((j)*(nc)) + (c) ])

int main(int argc, char** argv) {
    // bench_0();
    // bench_1();
    // bench_2();
    // bench_3();
    // bench_4();
    //  bench_5();
    bench_6();
   
  
         
    return 0;
}
double fops_calc(int N, int C, int K, int H, int W)
{
    // double fops;
    // //Filter ops
    // fops = (C*K*(18+14))/(10e9);
    // //Image transform ops
    // fops += (N*C*((W-2)/2000)*((H-2)/2000)*24)/(10e3);
    // //GEMM
    // fops += (N*C*K*((W-2)/2000)*((H-2)/2000)*2)/(10e3);
    // //Final transform
    // fops += (N*K*((W-2)/2)*((H-2)/2)*24)/(10e9);

    // // fops /= (10e9);

    return 1.0;
}

double bench(int N, int C, int K, int H, int W, int version)
{
    //                       n   c   h   w
    float* image = gen_image(N * C * H * W, 2.0);
    //                        k   c   r   s
    float* filter = gen_image(K * C * 3 * 3,     2.0);

    float *wnn_res = calloc(H*W*K*N , 4);

    float *image_pad;

    if(version < 2 || version == 11)
    {
       image_pad  = zero_pad(image, H, W, C, N);
    }else
    {
        image_pad  = zero_padNHWC(image, H, W, C, N);
    }
    
    
    double start, end;
    start = omp_get_wtime();
    WConv_3x3(image_pad, filter, wnn_res,  H+2, W+2, C, K, N, version);
    end = omp_get_wtime();
    
    
    free(image);
    free(filter);
    free(image_pad);
    free(wnn_res);
    fflush(NULL);

    return end-start;
}

float *zero_pad(float *image_r, int H, int W, int C, int N )
{
    int old_H,old_W;
    old_H = H;
    old_W = W;
    H = H + 2;
    W = W + 2;
    float *image = (float *) calloc(H*W*C*N, 4);
    for(int n=0; n < N; n++)
        for(int c=0; c < C; c++)
            for(int h=0; h < old_H; h++)
                for(int w=0; w < old_W; w++)
                {
                    image[(w + 1) + ((h + 1)*W) + (c*W*H) + (n*W*H*C)] = image_r[w + (h*old_W) + (c*old_W*old_H) + (n*old_W*old_H*C)];
                }
    return image;
}
  


float *zero_padNHWC(float *image_r, int H, int W, int C, int N )
{
    int old_H,old_W;
    old_H = H;
    old_W = W;
    H = H + 2;
    W = W + 2;
    float *image = (float *) calloc(H*W*C*N, 4);
      for(int n=0; n < N; n++)
        for(int c=0; c < C; c++)
            for(int h=0; h < old_H; h++)
                for(int w=0; w < old_W; w++)
                {
                    imageNHWC(n,c,(h+1),(w+1),   W,H,C) = image_rNHWC(n,c,h,w,  old_W, old_H,C);
                }
    return image;
}

int compare(float* image, float* res, int n)
{
    for(int i=0; i<n; i++) 
        if (abs(image[i] - res[i]) > 1e-5)
        {
            printf("\033[31mImages are NOT equal\033[39m\n");
            return 1;
        }
    
    printf("\033[32mImages are equal\033[39m\n");
    return 0;
}

float* gen_image(int n, float max_num)
{
    float *image = (float *) calloc(n,sizeof(float));
    for(int i=0; i< n; i++)  
        image[i] = ((float)rand()/(float)(RAND_MAX)) * max_num;
    return image; 
}

void gen_bench( int N, int C, int K, int H, int W, int version )
{
    double time = bench(N, C, K, H, W, version);
    printf("N%d, K%d, C%d, H%d, W%d | %0.2lf   |\n", N, K, C, H, W, time);
    fflush(stdout);
}

void bench_0()
{
     printf("\n\n\nBenchmark 0, check for correctness\n");

     int H_val[16] = {512 , 512, 513, 513, 512 , 512, 513, 513,
                   512 , 512, 513, 513, 512 , 512, 513, 513};
    int W_val[16] = {512 , 513, 512, 513, 512 , 513, 512, 513,
                   512 , 513, 512, 513, 512 , 513, 512, 513};
    int C_val[16] = {3   , 3  , 3  , 3,   4   , 4 ,  4  , 4,
                   3   , 3  , 3  , 3,   4   , 4 ,  4  , 4};
    int K_val[16] = {3   , 3  , 3  , 3,   3   , 3 ,  3  , 3,
                   4   , 4  , 4  , 4,   4   , 4 ,  4  , 4};
    int N = 22; 
    printf("\nOneBlock NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< 3; i++)
    {
        gen_bench(N, C_val[i], K_val[i], H_val[i], W_val[i], 0);
    }
    printf("\nTime ConvGEMM NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< 3; i++)
    {
        gen_bench(N, C_val[i], K_val[i], H_val[i], W_val[i], 1);
    }
    printf("\nTime OneBlock NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< 3; i++)
    {
       gen_bench(N, C_val[i], K_val[i], H_val[i], W_val[i], 2);
    }
    printf("\nTime ConvGEMM NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< 3; i++)
    {
       gen_bench(N, C_val[i], K_val[i], H_val[i], W_val[i], 3);
    }    

}


void bench_1()
{
    int H_val[3] = { 5422, 2012, 1006};
    int W_val[3] = { 5422, 2012, 1006};
    int C = 3;
    int N = 1;
    int K = 1; 
    
    printf("\n\n\nBenchmark 1, one image, various sizes, 3C, 1K (Filter convolution)\n");

    printf("\nOneBlock NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< 3; i++)
    {
        gen_bench(N, C, K, H_val[i], W_val[i], 0);
    }
    printf("\nTime ConvGEMM NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< 3; i++)
    {
        gen_bench(N, C, K, H_val[i], W_val[i], 1);    
    }
    printf("\nTime OneBlock NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< 3; i++)
    {
        gen_bench(N, C, K, H_val[i], W_val[i], 2);
    }
    printf("\nTime ConvGEMM NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< 3; i++)
    {
        gen_bench(N, C, K, H_val[i], W_val[i], 3);
    }    
    return;
}

void bench_2()
{
    int H = 412;
    int W = 412;
    int C = 1;
    int N = 20; 
    int k_start = 1;
    int k_times = 100;
    int k_inc = 20;
    
    printf("\n\n\nBenchmark 2, 20 images, fixed image size, 1C, variable K (Test K increase)\n");

    printf("\nOneBlock NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    for(int K=k_start; K< k_times; K+=k_inc)
    {
        gen_bench(N, C, K, H, W, 0);
    }
    printf("\nTime ConvGEMM NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    
    for(int K=k_start; K< k_times; K+=k_inc)
    {
        gen_bench(N, C, K, H, W, 1);
    }
    printf("\nTime OneBlock NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int K=k_start; K< k_times; K+=k_inc)
    {
        gen_bench(N, C, K, H, W, 2);
    }
    printf("\nTime ConvGEMM NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    
    for(int K=k_start; K< k_times; K+=k_inc)
    {
       gen_bench(N, C, K, H, W, 3);
    }    
    return;
}

void bench_3()
{
    int H = 312;
    int W = 312;
    int K = 1;
    int N = 20; 
    int c_start = 1;
    int c_times = 100;
    int c_inc = 20;
    
    printf("\n\n\nBenchmark 3, 20 images, fixed image size, 1K, variable C (Test C increase)\n");

    printf("\nOneBlock NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, K, H, W, 0);
    }
    printf("\nTime ConvGEMM NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, K, H, W, 1);
    }
    printf("\nTime OneBlock NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, K, H, W, 2);
    }
    printf("\nTime ConvGEMM NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    
    for(int C=c_start; C< c_times; C+=c_inc)
    {
       gen_bench(N, C, K, H, W, 3);
    }    
    return;
}

void bench_4()
{
    int H = 112;
    int W = 112;
    int N = 20; 
    int c_start = 1;
    int c_times = 100;
    int c_inc = 20;
    
    printf("\n\n\nBenchmark 4, 20 images, fixed image size, variable K, variable C (Test C and K increase)\n");

    printf("\nOneBlock NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, C, H, W, 0);
    }
    printf("\nTime ConvGEMM NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, C, H, W, 1);
    }
    printf("\nTime OneBlock NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, C, H, W, 2);
    }
    printf("\nTime ConvGEMM NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    
    for(int C=c_start; C< c_times; C+=c_inc)
    {
       gen_bench(N, C, C, H, W, 3);
    }    
    return;
}

void bench_5()
{
    int H = 512;
    int W = 512;
    int N = 1; 
    int c_start = 1;
    int c_times = 100;
    int c_inc = 20;
    
    printf("\n\n\nBenchmark 5, 1 image, fixed image size, variable K, variable C (Test C and K increase)\n");

    printf("\nOneBlock NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, C, H, W, 0);
    }
    printf("\nTime ConvGEMM NCHW: \n");
    printf("\t\t\t | TIME   |\n");
    
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, C, H, W, 1);
    }
    printf("\nTime OneBlock NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int C=c_start; C< c_times; C+=c_inc)
    {
        gen_bench(N, C, C, H, W, 2);
    }
    printf("\nTime ConvGEMM NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    
    for(int C=c_start; C< c_times; C+=c_inc)
    {
       gen_bench(N, C, C, H, W, 3);
    }    
    return;
}


void bench_6()
{
    int N_val[9] = { 30,  30,  30,  30,  30,  30,  30,  30,  30};
    int C_val[9] = {  3,  64,  64, 128, 128, 256, 256, 512, 512};
    int K_val[9] = { 64,  64, 128, 128, 256, 256, 512, 512, 512};
    int H_val[9] = {224, 224, 112, 112,  56,  56,  28,  28, 14};
    int W_val[9] = {224, 224, 112, 112,  56,  56,  28,  28, 14};
    int ser = 9;
     
    
    printf("\n\n\nBenchmark 6, VGG16 Imagenet layers\n");

    // printf("\nOneBlock NCHW: \n");
    // printf("\t\t\t | TIME   |\n");
    // for(int i=0; i< ser; i++)
    // {
    //     gen_bench(N_val[i], C_val[i], K_val[i], H_val[i], W_val[i], 0);
    // }
    // printf("\nTime ConvGEMM NCHW: \n");
    // printf("\t\t\t | TIME   |\n");
    // for(int i=0; i< ser; i++)
    // {
    //     gen_bench(N_val[i], C_val[i], K_val[i], H_val[i], W_val[i], 1);    
    // }
    // printf("\nTime OneBlock NHWC: \n");
    // printf("\t\t\t | TIME   |\n");
    // for(int i=0; i< ser; i++)
    // {
    //     gen_bench(N_val[i], C_val[i], K_val[i], H_val[i], W_val[i], 2);
    // }
    printf("\nTime ConvGEMM NHWC: \n");
    printf("\t\t\t | TIME   |\n");
    for(int i=0; i< ser; i++)
    {
        gen_bench(N_val[i], C_val[i], K_val[i], H_val[i], W_val[i], 3);
    }
    // printf("\nTime ConvGEMM BULK NCHW: \n");
    // printf("\t\t\t | TIME   |\n");
    // for(int i=0; i< ser; i++)
    // {
    //     gen_bench(N_val[i], C_val[i], K_val[i], H_val[i], W_val[i], 11);    
    // }
    // printf("\nTime ConvGEMM BULK NHWC: \n");
    // printf("\t\t\t | TIME   |\n");
    // for(int i=0; i< ser; i++)
    // {
    //     gen_bench(N_val[i], C_val[i], K_val[i], H_val[i], W_val[i], 33);
    // }
    return;
}