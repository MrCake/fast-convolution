#!/bin/bash

# sync; echo 1 > /proc/sys/vm/drop_caches
# sync; echo 2 > /proc/sys/vm/drop_caches
# sync; echo 3 > /proc/sys/vm/drop_caches 

#gcc -O3 -g -o dcnn_pGH paperGEMMHadamard/dcnn.c -lm -lblas -lgomp 
#gcc -O3 -g -o dcnn_pGHNHWC paperGEMMHadamardNHWC/dcnn.c -lm -lblas -lgomp 
cc -O3 -fPIC -o dcnn_pGHNHWC paperGEMMHadamardNHWC/dcnn.c -I/nfs/alumnos/djcuagee/blis/include/blis /nfs/alumnos/djcuagee/blis/lib/libblis.a  -lblas -lm -lgomp -lpthread  

# n c k h w
arg="30 512 512 28 28"
fold="out_n30_c512_k512_h28_w28_blis"

mkdir $fold

for (( i=1; i<=$1; i++ ))
do
	echo -e "\nTime "  $i
	#echo -e "\nTime "  $i >> $fold/out_paperGEMMHadamard
	echo -e "\nTime "  $i >> $fold/out_paperGEMMHadamardNHWC

	sleep 2
	#./dcnn_pGH $arg >> $fold/out_paperGEMMHadamard
	./dcnn_pGHNHWC $arg >> $fold/out_paperGEMMHadamardNHWC
	sleep 2
done
