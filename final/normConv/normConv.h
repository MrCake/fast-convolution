#define res(n, k, i, j,    w, h, nk) (res[ (n*(nk*h*w)) + (k*(w*h)) + ((i)*(w)+(j))])

#define image(n, c, i, j,    w, h, nc) (image[ (j) + ((i)*(w)) + ((c)*(w*h)) + ((n)*(nc*h*w))])
#define filter(k, c, i, j,    w, h, nk) (filter[ (c*(nk*h*w)) + (k*(w*h)) + ((i)*(w)+(j))])


#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))


/*
    Input:
    - float* image: 1dimensional image with size N*H*W*C
    - float* filter: 1dimensional kernel/filter to apply to image with size K*R*S*C
    - int h: height of the image
    - int w: width of the image
    - int r: height of the kernel
    - int s: width of the kernel
    - int c: number of channels in image (r,g,b = 3 - b,w = 1)
    - int k: number of kernels
    - int n: batch size to process

    Output:
    - float* res: resolution of convolution with size n*k*h*w

    Notes:
    - image and filter are using NCHW format
    - over 3x3 filter and 4x4 image block
*/
float* normConv(float* image,float* filter, int H, int W, int R, int S, int C, int K, int N)
{
    float *res = (float *) calloc(H*W*K*N, sizeof(float));

    int ir, js;
    #pragma omp parallel for
    for(int n = 0; n < N; n++)
        for(int k = 0; k < K; k++)
            for(int c = 0; c < C; c++)
                for(int i = 0; i < H; i++)
                    for(int j = 0; j < W; j++)
                        for(int r = 0; r < R; r++)
                           for(int s = 0; s < S; s++)
                            {
                                ir = i+r - (R/2);
                                js = j+s - (S/2);
                                if( (ir>=0) && (ir<=H-1) && (js>=0) && (js<=W-1))
                                    res(n,k,i,j,   W,H,K) += image(n,c,ir,js,   W,H,C) * filter(k,c,r,s,  S,R,K);
                            } 
    return res;
}