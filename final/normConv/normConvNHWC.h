// NHWC
#define imageNHWC(n, c, i, j,    w, h, nc) (image[ ((n)*(nc*h*w)) + ((i)*(w*nc)) + ((j)*(nc)) + (c) ])
//CRSK
#define filterCRSK(k, c, i, j,    w, h, nk) (filter[ (k) + ((j)*nk) + ((i)*nk*w) + ((c)*nk*w*h) ]) 


//NHWK
#define resNHWK(n, k, i, j,    w, h, nk) (res[ (n*(nk*h*w)) + ((i)*((w)*nk)+((j)*nk)) + (k)])


float* normConvNHWC(float* image,float* filter, int H, int W, int R, int S, int C, int K, int N)
{
    float *res = (float *) calloc(H*W*K*N, sizeof(float));

    int ir, js;
    #pragma omp parallel for
    for(int n = 0; n < N; n++)
        for(int k = 0; k < K; k++)
            for(int c = 0; c < C; c++)
                for(int i = 0; i < H; i++)
                    for(int j = 0; j < W; j++)
                        for(int r = 0; r < R; r++)
                           for(int s = 0; s < S; s++)
                            {
                                ir = i+r - (R/2);
                                js = j+s - (S/2);
                                if( (ir>=0) && (ir<=H-1) && (js>=0) && (js<=W-1))
                                    resNHWK(n,k,i,j,   W,H,K) += imageNHWC(n,c,ir,js,   W,H,C) * filterCRSK(k,c,r,s,  S,R,K);
                            } 
    return res;
}