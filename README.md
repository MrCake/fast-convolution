# TFM: Winograds Fast Convolution for ARM devices

This repository contains all the code that was used to complete my masters thesis. It is devided in the following sections:

- *final*: Code for fast convolution using Winograds algorithm. Only a 3x3 filter has been implemented. There are three versions:
    - OneBlock: Naive implementation of Winograds algorithm (memory efficient)
    - ConvGEMM: Implementation based on the paper by Partha M. and others,  [Efficient Winograd or Cook-Toom Convolution Kernel Implementation on Widely Used Mobile CPUs](https://arxiv.org/abs/1903.01521), making use of matrix product to calculate minimal convolution.
    - ConvGEMMBulk: Extension of ConvGEMM that processes a batch of images at the same time.
- *plot*: python notebooks used to create plots (with matplotlib).


To run the code in 'final' folder, [*BLIS*](https://github.com/flame/blis) must be installed and the respective Makefiles in '/final' and '/final/WConv_3x3' must be modified to include the path were BLIS was installed.

Once BLIS has been included properly, to compile the 'library' do:

```
cd final/WConv_3x3/ ;
make

```

Finally to check if the convolution is calculated correctly:

```
cd final/ ;
make check ;
./WConv_3x3test
```

And to execute predefined benchmarks:

```
cd final/ ;
make bench ;
./WConv_3x3bench
```

